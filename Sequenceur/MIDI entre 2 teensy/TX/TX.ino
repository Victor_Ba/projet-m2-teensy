#include "MIDI.h"

MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);
int etat = LOW;

void setup() {
  MIDI.begin(MIDI_CHANNEL_OMNI);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  MIDI.sendNoteOn(53, 127, 3);
  digitalWrite(LED_BUILTIN, etat);
  etat = !etat;
  delay(500);
}
