#include "MIDI.h"

MIDI_CREATE_INSTANCE(HardwareSerial, Serial2, MIDI);
int etat = LOW;

void setup() {
  MIDI.begin(MIDI_CHANNEL_OMNI);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.begin(57600);
}

void loop() {
  int type, note, velocity, channel, d1, d2;
  if (MIDI.read()) 
  {
    byte type = MIDI.getType();
    if(type==midi::NoteOn) {
      note = MIDI.getData1();
      velocity = MIDI.getData2();
      channel = MIDI.getChannel();
      if (velocity > 0) {
      Serial.println(String("Note On:  ch=") + channel + ", note=" + note + ", velocity=" + velocity);
      }
    }   
    etat = !etat;
    digitalWrite(LED_BUILTIN, etat);
  }
}
