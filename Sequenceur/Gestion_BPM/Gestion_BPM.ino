#include <TimerOne.h>
#include <MIDI.h>

/////////////////////////// CONSTANTES //////////////////////
const int led = LED_BUILTIN;
const int valeur_BPM = A9;
MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);

/////////////////////////// VARIABLES GLOBALES //////////////////////
int last_BPM = 120;
volatile int ledState = LOW;

/////////////////////////// FONCTIONS //////////////////////
/**
  * @exception  Incremente l'horloge interne du séquenceur, envoie les messages MIDI.
  */
void send_MIDI(void)
{
  static unsigned int compteur = 1;
  compteur++;
  if(compteur==24)
  {
  // Clignotage led
  ledState = !ledState;
  digitalWrite(led, ledState);
  // Clignotage led
  
  MIDI.sendNoteOn(64, 127, 3);
  compteur = 0;
  }
}

/**
  * @fn int conv_BPM_us(int _BPM)
  * @brief Convertit la valeur du nombre de BPM (Battements par minute) en une période en us.
  * @param _BPM: le nombre de battements par minute.
  */
int conv_BPM_us(int _BPM)
{
  float frequence_interruption;
  float periode_interruption_us;
  frequence_interruption = float(float(_BPM)*float(24)/float(60));
  periode_interruption_us = 1.0/frequence_interruption*1000000;
  return int(periode_interruption_us);
  
}

/**
  * @fn void gestion_BPM()
  * @brief Gère l'acquisition du nombre de BPM, sa conversion et la mise à jour de l'horloge du séquenceur si nécessaire.
  */
void gestion_BPM()
{
  int BPM;
  int BPM_us;  

  BPM = analogRead(valeur_BPM);  
  BPM = map(BPM, 1, 1024, 20, 180);
  
  if(BPM != last_BPM)
  {
    Serial.println("BPM = ");
    Serial.println(BPM);
    BPM_us = conv_BPM_us(BPM);
    Timer1.setPeriod(BPM_us);
    last_BPM = BPM;
  }
}

/////////////////////////// MAIN //////////////////////
void setup(void)
{
  MIDI.begin(MIDI_CHANNEL_OMNI);
  pinMode(led, OUTPUT);
  Timer1.initialize(50000);
  Timer1.attachInterrupt(send_MIDI);
  Serial.begin(9600);
}

void loop(void)
{
  gestion_BPM();
  delay(500);

}
