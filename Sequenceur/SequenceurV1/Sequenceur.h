/**
  ******************************************************************************
  * @file           : Sequenceur.h
  * @brief          : Header du fichier Sequenceur.c
					Regroupe les variables et les prototypes des fonctions du sequenceur MIDI
  ******************************************************************************
  */
#ifndef SEQUENCEUR_H 
#define SEQUENCEUR_H

#include <Arduino.h>
#include <TimerOne.h>
#include <MIDI.h>
#include <stdint.h>

/////////////////////////// CONSTANTES /////////////////////////////
const int led = LED_BUILTIN;
const int PotVelocite = A8;
const int NoteOnButton = 14;
const int valeur_BPM = A9;
const int MidiChannelPot = A7;
const int ClkDividerPot = A6;

const byte NB_SEQUENCE = 4;
const byte CLK_DIVIDER_MAX = 24;
const byte LG_SEQUENCE = CLK_DIVIDER_MAX * NB_SEQUENCE;
const byte MIDI_CHANNEL_MAX = 16;
const byte ClkDividerSetup[8] = {1, 2, 3, 4, 6, 8, 12, 24};

/////////////////////////// VARIABLES GLOBALES //////////////////////
extern volatile byte velocite[MIDI_CHANNEL_MAX][LG_SEQUENCE];  // = {127, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 60, 0, 0, 0, 0, 0};
extern volatile byte PosSeq;
extern volatile int CurrentClkDivider;
extern int CurrentMidiChannel;
extern unsigned long tps;

/////////////////////////// PROTOTYPES DES FONCTIONS /////////////////
void send_MIDI();
int conv_BPM_us(int _BPM);
void gestion_BPM();
void PlaySequence();
void SetCurrentMidiChannel();
void SetClkDividerSetup();
void InitMidi();
void InitVelo();
void InitTimer();
void InitIO();
void InitCom();

#endif
