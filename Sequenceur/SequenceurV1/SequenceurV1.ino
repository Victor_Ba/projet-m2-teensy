#include "Sequenceur.h"
unsigned long tps2 = 0;

void setup() {
  InitCom();
  InitIO();
  InitVelo();
  InitTimer();
}

void loop() {
  if(millis() - tps > 200)
  {
    gestion_BPM();
    SetCurrentMidiChannel();
    SetClkDividerSetup();
    tps = millis();
  }
  PlaySequence();
}
