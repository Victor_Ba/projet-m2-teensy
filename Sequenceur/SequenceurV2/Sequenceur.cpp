/**
  ******************************************************************************
  * @file           : Sequenceur.c
  * @brief          : Définition des fonctions du sequenceur MIDI
  ******************************************************************************
  */

#include "Sequenceur.h"

MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);
volatile byte PosSeq = -1;
volatile int CurrentClkDivider = 0;
int CurrentMidiChannel = 1;
unsigned long tps = 0;
volatile byte velocite[MIDI_CHANNEL_MAX][LG_SEQUENCE];

/**
  * @exception  Incremente l'horloge interne du séquenceur. Met à jour le tableau de vélocité de la note joué si nécessaire.
  */
void send_MIDI()
{
  PosSeq++;
  if(PosSeq >= LG_SEQUENCE) PosSeq = 0;

  // Si l'utilisateur appuie sur le bouton et que la position courante dans la séquence = un multiple la division temporelle choisie
  // on met à jour le tableau de vélocité avec la nouvelle valeur.
  if(!(digitalRead(NoteOnButton)) && PosSeq%(CLK_DIVIDER_MAX/ClkDividerSetup[CurrentClkDivider])==0)
  {
    byte velo;
    velo = byte(map(analogRead(PotVelocite), 0, 1023, 0, 127));
    velocite[CurrentMidiChannel][PosSeq] = velo;
  }
}

/**
  * @fn int conv_BPM_us(int _BPM)
  * @brief Convertit la valeur du nombre de BPM (Battements par minute) en une période en us.
  * @param _BPM: le nombre de battements par minute.
  */
int conv_BPM_us(int _BPM)
{
  float frequence_interruption;
  float periode_interruption_us;
  frequence_interruption = float(float(_BPM)*float(24)/float(60));
  periode_interruption_us = 1.0/frequence_interruption*1000000;
  return int(periode_interruption_us);
}

/**
  * @fn void gestion_BPM()
  * @brief Gère l'acquisition du nombre de BPM, sa conversion en microsecondes et la mise à jour de l'horloge du séquenceur si nécessaire.
  */
void gestion_BPM()
{
  int _BPM;
  int _BPM_us;
  static int _last_BPM = 120;

  _BPM = analogRead(valeur_BPM);  
  _BPM = map(_BPM, 0, 1023, 20, 180);
  
  if(_BPM != _last_BPM)
  {
    Serial.println("BPM = ");
    Serial.println(_BPM);
    _BPM_us = conv_BPM_us(_BPM);
    Timer1.setPeriod(_BPM_us);
    _last_BPM = _BPM;
  }
}

/**
  * @fn void PlaySequence()
  * @brief Envoie les notes Midi correspondant à la position actuelle dans la séquence.
  */
void PlaySequence()
{
  static byte _LastPosConnue = -1;
  static int _ledState = LOW;
  if(PosSeq != _LastPosConnue)
  {
    _ledState = !_ledState;
    digitalWrite(led, _ledState);

    for(byte _canal=0;_canal<MIDI_CHANNEL_MAX;_canal++)
    {
      MIDI.sendNoteOn(64, velocite[_canal][PosSeq], _canal);
    }
    
    _LastPosConnue = PosSeq;
  }  
}

/**
  * @fn void SetCurrentMidiChannel()
  * @brief Sélectionne le canal MIDI.
  */
void SetCurrentMidiChannel()
{
  int _MidiChannel;
  _MidiChannel = analogRead(MidiChannelPot);
  _MidiChannel = map(_MidiChannel, 0, 1023, 1, 16);

  if(CurrentMidiChannel != _MidiChannel)
  {
    CurrentMidiChannel = _MidiChannel;
    Serial.println("Current Midi Channel");
    Serial.println(CurrentMidiChannel);
  }
}

/**
  * @fn void SetClkDividerSetup()
  * @brief Règle la division temporelle.
  */
void SetClkDividerSetup()
{
  int _ClkDividerSetup;
  _ClkDividerSetup = analogRead(ClkDividerPot);
  _ClkDividerSetup = map(_ClkDividerSetup, 0, 1023, 0, 7);

  if(CurrentClkDivider != _ClkDividerSetup)
  {
    CurrentClkDivider = _ClkDividerSetup;
    Serial.println("Current Clk Divider");
    Serial.println(CLK_DIVIDER_MAX/ClkDividerSetup[CurrentClkDivider]);
  }
}

/**
  * @fn void InitMidi()
  * @brief Permet d'assigner un canal midi à un instrument.
  */
void InitMidi()
{
  while(digitalRead(NoteOnButton))
  {
    Serial.println("attente synchro midi...");
    delay(250);
  }
  for(byte _canal=0;_canal<MIDI_CHANNEL_MAX;_canal++)
  {
    MIDI.sendNoteOn(64, 127, _canal);
    digitalWrite(led, HIGH);
    delay(125);
    digitalWrite(led, LOW);
    delay(125);
    digitalWrite(led, HIGH);
    delay(125);
    digitalWrite(led, LOW);
    delay(125);
  }
}

/**
  * @fn void InitVelo()
  * @brief Initialise le tableau de vélocité à 0.
  */
void InitVelo()
{
  for(byte _canal=0;_canal<MIDI_CHANNEL_MAX;_canal++)
  {
    for(byte _pos=0;_pos<LG_SEQUENCE;_pos++)
    {
      velocite[_canal][_pos] = 0;
    }
  }
}

/**
  * @fn void InitTimer()
  * @brief Initialise et démarre le timer1 à 50ms, appel la fonction send_MIDI sur l'interruption.
  */
void InitTimer()
{
	Timer1.initialize(50000);
	Timer1.attachInterrupt(send_MIDI);
}

/**
  * @fn void InitIO()
  * @brief Initialise les entrées/sorties de la carte.
  */
void InitIO()
{
	pinMode(led, OUTPUT);
	pinMode(NoteOnButton, INPUT_PULLUP);
}

/**
  * @fn void InitCom()
  * @brief Initialise la liaison série pour la communication avec le pc et la liaison MIDI pour la communication avec le sampler.
  */
void InitCom()
{
	MIDI.begin();
	Serial.begin(9600);
}
