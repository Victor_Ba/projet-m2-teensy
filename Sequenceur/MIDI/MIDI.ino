#include <TimerOne.h>
#include <MIDI.h>

/////////////////////////// CONSTANTES //////////////////////
const int led = LED_BUILTIN;
const int PotVelocite = A8;
const int NoteOnButton = 14;
const int valeur_BPM = A9;
const int MidiChannelPot = A7;
const byte MIDI_CHANNEL_MAX = 16;
const byte CLK_DIVIDER_MAX = 24;
const byte ClkDividerSetup[8] = {1, 2, 3, 4, 6, 8, 12, 24};

/////////////////////////// VARIABLES GLOBALES //////////////////////
MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);
volatile byte velocite[MIDI_CHANNEL_MAX][CLK_DIVIDER_MAX];  // = {127, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 60, 0, 0, 0, 0, 0};
volatile byte PosSeq = -1;
int CurrentMidiChannel = 1;
unsigned long tps;

/////////////////////////// FONCTIONS //////////////////////
/**
  * @exception  Incremente l'horloge interne du séquenceur. Met à jour le tableau de vélocité de la note joué si nécessaire.
  */
void send_MIDI(void)
{
  PosSeq++;
  if(PosSeq > 23) PosSeq = 0;

  // Si l'utilisateur appuie sur le bouton et que la position courante dans la séquence = un multiple la division temporelle choisi
  // on met à jour le tableau de vélocité avec la nouvelle valeur.
  if(!(digitalRead(NoteOnButton)) && PosSeq%(CLK_DIVIDER_MAX/ClkDividerSetup[0])==0)
  {
    byte velo;
    velo = byte(map(analogRead(PotVelocite), 0, 1023, 0, 127));
    Serial.println(velo);
    velocite[CurrentMidiChannel][PosSeq] = velo;
  }
}

/**
  * @fn int conv_BPM_us(int _BPM)
  * @brief Convertit la valeur du nombre de BPM (Battements par minute) en une période en us.
  * @param _BPM: le nombre de battements par minute.
  */
int conv_BPM_us(int _BPM)
{
  float frequence_interruption;
  float periode_interruption_us;
  frequence_interruption = float(float(_BPM)*float(24)/float(60));
  periode_interruption_us = 1.0/frequence_interruption*1000000;
  return int(periode_interruption_us);
}

/**
  * @fn void gestion_BPM()
  * @brief Gère l'acquisition du nombre de BPM, sa conversion en microsecondes et la mise à jour de l'horloge du séquenceur si nécessaire.
  */
void gestion_BPM()
{
  int _BPM;
  int _BPM_us;
  static int _last_BPM = 120;

  _BPM = analogRead(valeur_BPM);  
  _BPM = map(_BPM, 1, 1024, 20, 180);
  
  if(_BPM != _last_BPM)
  {
    Serial.println("BPM = ");
    Serial.println(_BPM);
    _BPM_us = conv_BPM_us(_BPM);
    Timer1.setPeriod(_BPM_us);
    _last_BPM = _BPM;
  }
}

/**
  * @fn void PlaySequence()
  * @brief Envoie les notes Midi correspondant à la position actuelle dans la séquence.
  */
void PlaySequence()
{
  static byte _LastPosConnue = -1;
  static int _ledState = LOW;
  if(PosSeq != _LastPosConnue)
  {
    _ledState = !_ledState;
    digitalWrite(led, _ledState);

    for(byte _canal=0;_canal<MIDI_CHANNEL_MAX;_canal++)
    {
      MIDI.sendNoteOn(64, velocite[_canal][PosSeq], _canal+1);
    }
    
    _LastPosConnue = PosSeq;
  }  
}

/**
  * @fn void SetCurrentMidiChannel()
  * @brief Sélectionne le canal MIDI.
  */
void SetCurrentMidiChannel()
{
  int _MidiChannel;
  _MidiChannel = analogRead(MidiChannelPot);
  _MidiChannel = map(_MidiChannel, 0, 1024, 1, 16);

  if(CurrentMidiChannel != _MidiChannel)
  {
    CurrentMidiChannel = _MidiChannel;
    Serial.println("Current Midi Channel");
    Serial.println(CurrentMidiChannel);
  }
}

/**
  * @fn void InitMidi()
  * @brief Permet d'assigner un canal midi à un instrument.
  */
void InitMidi()
{
  while(digitalRead(NoteOnButton))
  {
    Serial.println("attente synchro midi...");
    delay(250);
  }
  for(byte _canal=1;_canal<=MIDI_CHANNEL_MAX;_canal++)
  {
    MIDI.sendNoteOn(64, 127, _canal);
    digitalWrite(led, HIGH);
    delay(125);
    digitalWrite(led, LOW);
    delay(125);
    digitalWrite(led, HIGH);
    delay(125);
    digitalWrite(led, LOW);
    delay(125);
  }
}

/**
  * @fn void InitVelo()
  * @brief Initialise le tableau de vélocité à 0.
  */
void InitVelo()
{
  for(byte _canal=0;_canal<MIDI_CHANNEL_MAX;_canal++)
  {
    for(byte _pos=0;_pos<CLK_DIVIDER_MAX;_pos++)
    {
      velocite[_canal][_pos] = 0;
    }
  }
}

/////////////////////////// MAIN //////////////////////
void setup(void)
{
  MIDI.begin(MIDI_CHANNEL_OMNI);
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(NoteOnButton, INPUT_PULLUP);
  InitVelo();
  //InitMidi();
  Timer1.initialize(50000);
  Timer1.attachInterrupt(send_MIDI);
}

void loop(void)
{
  if(millis() - tps > 200)
  {
    gestion_BPM();
    SetCurrentMidiChannel();
    tps = millis();
  }
  PlaySequence();
}
