#include <Wire.h>
#include <Audio.h>
#include <Synth-Clatter.h>
#include <Synth-Decay.h>
#include <Synth-DrumHeart.h>

///////////////////////////////////////////
// Declaration des objets et connections //
///////////////////////////////////////////

AudioSynthClatter      	 clat1;
AudioSynthDecay        	 DECAY_HAT;
AudioSynthDecay        	 DECAY_CYMBAL;
AudioFilterBiquad        FILTER_HAT;
AudioFilterBiquad        FILTER_CYMBAL;
AudioEffectMultiply      MUL_HAT;
AudioEffectMultiply      MUL_CYMBAL;
AudioSynthSimpleDrum     drum1;          //xy=114,585
AudioSynthSimpleDrum     drum4;          //xy=114,759
AudioPlaySdWav           playWav1;     //xy=131,823
AudioPlaySdWav           playWav2;     //xy=131,868
AudioPlaySdWav           playWav3; //xy=137.75,919.75
AudioPlaySdWav           playWav4; //xy=137.75,964.75
AudioEffectGranular      granular1;      //xy=317,821
AudioMixer4              PERCU_ENVOI_EFFET1;         //xy=322,114
AudioEffectGranular      granular3; //xy=323.75,917.75
AudioMixer4              PERCU_ENVOI_EFFET2; //xy=327,187
AudioMixer4              PERCU_ENVOI_EFFET3; //xy=329,263
AudioMixer4              SAMPLE_ENVOI_EFFET1; //xy=331.75,362.75
AudioMixer4              SAMPLE_ENVOI_EFFET2; //xy=334.75,435.75
AudioEffectGranular      granular2;      //xy=334,869
AudioMixer4              SAMPLE_ENVOI_EFFET3; //xy=337.75,507.75
AudioEffectGranular      granular4; //xy=340.75,965.75
AudioAmplifier           REGLAGE_VOLUME_MUL_CYMBAL; //xy=394,695
AudioAmplifier           REGLAGE_VOLUME_MUL_HAT; //xy=395,647
AudioAmplifier           REGLAGE_VOLUME_DRUM1; //xy=401,588
AudioAmplifier           REGLAGE_VOLUME_DRUM4; //xy=409,734
AudioMixer4				 MIX_WAV1;
AudioMixer4				 MIX_WAV2;
AudioMixer4				 MIX_WAV3;
AudioMixer4				 MIX_WAV4;
AudioEffectEnvelope      envelope1;      //xy=503,820
AudioEffectEnvelope      envelope2; //xy=508,868
AudioEffectEnvelope      envelope3; //xy=509.75,916.75
AudioEffectEnvelope      envelope4; //xy=514.75,964.75
AudioMixer4              MIX_ENVOI_EFFET1;         //xy=600.75,233.75
AudioMixer4              MIX_ENVOI_EFFET3; //xy=604.75,393.75
AudioMixer4              MIX_ENVOI_EFFET2; //xy=607.75,316.75
AudioMixer4              SAMPLE_GAUCHE;         //xy=708,798
AudioMixer4              SAMPLE_DROITE; //xy=711,882
AudioMixer4              PERCU_DROITE; //xy=719,694
AudioMixer4              PERCU_GAUCHE;         //xy=721,604
AudioFilterBiquad        biquad1;        //xy=796,423
AudioEffectBitcrusher    bitcrusher1;    //xy=805,354
AudioEffectReverb        reverb1;        //xy=814,253
AudioMixer4              EFFET_DROITE; //xy=1065,409
AudioMixer4              EFFET_GAUCHE; //xy=1067,319
AudioMixer4              MASTER_DROITE; //xy=1068,792
AudioMixer4              MASTER_GAUCHE;         //xy=1069,718
AudioOutputI2S           i2s1;           //xy=1277,752
AudioControlSGTL5000     sgtl5000_1;     //xy=844,948
// GUItool: end automatically generated code


AudioConnection       	 patchCord75(clat1, 0 ,FILTER_HAT,0);
AudioConnection       	 patchCord76(FILTER_HAT, 0 ,MUL_HAT,0);
AudioConnection       	 patchCord77(DECAY_HAT, 0 ,MUL_HAT,1);

AudioConnection       	 patchCord78(clat1, 0 ,FILTER_CYMBAL,0);
AudioConnection       	 patchCord79(FILTER_CYMBAL, 0 ,MUL_CYMBAL,0);
AudioConnection       	 patchCord80(DECAY_CYMBAL, 0 ,MUL_CYMBAL,1);

AudioConnection          patchCord1(MUL_HAT, REGLAGE_VOLUME_MUL_HAT);
AudioConnection          patchCord2(MUL_HAT, 0, PERCU_ENVOI_EFFET1, 1);
AudioConnection          patchCord3(MUL_HAT, 0, PERCU_ENVOI_EFFET2, 1);
AudioConnection          patchCord4(MUL_HAT, 0, PERCU_ENVOI_EFFET3, 1);
AudioConnection          patchCord5(MUL_HAT, 0, PERCU_ENVOI_EFFET3, 2);
AudioConnection          patchCord6(drum1, 0, PERCU_ENVOI_EFFET1, 0);
AudioConnection          patchCord7(drum1, 0, PERCU_ENVOI_EFFET2, 0);
AudioConnection          patchCord8(drum1, 0, PERCU_ENVOI_EFFET3, 0);
AudioConnection          patchCord9(drum1, REGLAGE_VOLUME_DRUM1);
AudioConnection          patchCord10(drum4, REGLAGE_VOLUME_DRUM4);
AudioConnection          patchCord11(drum4, 0, PERCU_ENVOI_EFFET1, 3);
AudioConnection          patchCord12(drum4, 0, PERCU_ENVOI_EFFET2, 3);
AudioConnection          patchCord13(drum4, 0, PERCU_ENVOI_EFFET3, 3);
AudioConnection          patchCord14(MUL_CYMBAL, REGLAGE_VOLUME_MUL_CYMBAL);
AudioConnection          patchCord15(MUL_CYMBAL, 0, PERCU_ENVOI_EFFET1, 2);
AudioConnection          patchCord16(MUL_CYMBAL, 0, PERCU_ENVOI_EFFET2, 2);
AudioConnection          patchCord81(playWav1, 0, MIX_WAV1,0);
AudioConnection          patchCord82(playWav1, 1, MIX_WAV1,1);
AudioConnection          patchCord17(MIX_WAV1, granular1);
AudioConnection          patchCord18(playWav1, 0, SAMPLE_ENVOI_EFFET1, 0);
AudioConnection          patchCord19(playWav1, 0, SAMPLE_ENVOI_EFFET2, 0);
AudioConnection          patchCord20(playWav1, 0, SAMPLE_ENVOI_EFFET3, 0);
AudioConnection          patchCord83(playWav2, 0, MIX_WAV2,0);
AudioConnection          patchCord84(playWav2, 1, MIX_WAV2,1);
AudioConnection          patchCord21(MIX_WAV2, granular2);
AudioConnection          patchCord22(playWav2, 0, SAMPLE_ENVOI_EFFET1, 1);
AudioConnection          patchCord23(playWav2, 0, SAMPLE_ENVOI_EFFET2, 1);
AudioConnection          patchCord24(playWav2, 0, SAMPLE_ENVOI_EFFET3, 1);
AudioConnection          patchCord85(playWav3, 0, MIX_WAV3,0);
AudioConnection          patchCord86(playWav4, 1, MIX_WAV3,1);
AudioConnection          patchCord25(MIX_WAV3, granular3);
AudioConnection          patchCord26(playWav3, 0, SAMPLE_ENVOI_EFFET1, 2);
AudioConnection          patchCord27(playWav3, 0, SAMPLE_ENVOI_EFFET2, 2);
AudioConnection          patchCord28(playWav3, 0, SAMPLE_ENVOI_EFFET3, 2);
AudioConnection          patchCord87(playWav4, 0, MIX_WAV4,0);
AudioConnection          patchCord88(playWav4, 1, MIX_WAV4,1);
AudioConnection          patchCord29(MIX_WAV4, granular4);
AudioConnection          patchCord30(playWav4, 0, SAMPLE_ENVOI_EFFET1, 3);
AudioConnection          patchCord31(playWav4, 0, SAMPLE_ENVOI_EFFET2, 3);
AudioConnection          patchCord32(playWav4, 0, SAMPLE_ENVOI_EFFET3, 3);
AudioConnection          patchCord33(granular1, envelope1);
AudioConnection          patchCord34(PERCU_ENVOI_EFFET1, 0, MIX_ENVOI_EFFET1, 0);
AudioConnection          patchCord35(granular3, envelope3);
AudioConnection          patchCord36(PERCU_ENVOI_EFFET2, 0, MIX_ENVOI_EFFET2, 0);
AudioConnection          patchCord37(PERCU_ENVOI_EFFET3, 0, MIX_ENVOI_EFFET3, 0);
AudioConnection          patchCord38(SAMPLE_ENVOI_EFFET1, 0, MIX_ENVOI_EFFET1, 1);
AudioConnection          patchCord39(SAMPLE_ENVOI_EFFET2, 0, MIX_ENVOI_EFFET2, 1);
AudioConnection          patchCord40(granular2, envelope2);
AudioConnection          patchCord41(SAMPLE_ENVOI_EFFET3, 0, MIX_ENVOI_EFFET3, 1);
AudioConnection          patchCord42(granular4, envelope4);
AudioConnection          patchCord43(REGLAGE_VOLUME_MUL_CYMBAL, 0, PERCU_DROITE, 2);
AudioConnection          patchCord44(REGLAGE_VOLUME_MUL_CYMBAL, 0, PERCU_GAUCHE, 2);
AudioConnection          patchCord45(REGLAGE_VOLUME_MUL_HAT, 0, PERCU_GAUCHE, 1);
AudioConnection          patchCord46(REGLAGE_VOLUME_MUL_HAT, 0, PERCU_DROITE, 1);
AudioConnection          patchCord47(REGLAGE_VOLUME_DRUM1, 0, PERCU_GAUCHE, 0);
AudioConnection          patchCord48(REGLAGE_VOLUME_DRUM1, 0, PERCU_DROITE, 0);
AudioConnection          patchCord49(REGLAGE_VOLUME_DRUM4, 0, PERCU_GAUCHE, 3);
AudioConnection          patchCord50(REGLAGE_VOLUME_DRUM4, 0, PERCU_DROITE, 3);
AudioConnection          patchCord51(envelope1, 0, SAMPLE_GAUCHE, 0);
AudioConnection          patchCord52(envelope1, 0, SAMPLE_DROITE, 0);
AudioConnection          patchCord53(envelope2, 0, SAMPLE_GAUCHE, 1);
AudioConnection          patchCord54(envelope2, 0, SAMPLE_DROITE, 1);
AudioConnection          patchCord55(envelope3, 0, SAMPLE_GAUCHE, 2);
AudioConnection          patchCord56(envelope3, 0, SAMPLE_DROITE, 2);
AudioConnection          patchCord57(envelope4, 0, SAMPLE_DROITE, 3);
AudioConnection          patchCord58(MIX_ENVOI_EFFET1, reverb1);
AudioConnection          patchCord59(MIX_ENVOI_EFFET3, biquad1);
AudioConnection          patchCord60(MIX_ENVOI_EFFET2, bitcrusher1);
AudioConnection          patchCord61(SAMPLE_GAUCHE, 0, MASTER_GAUCHE, 2);
AudioConnection          patchCord62(SAMPLE_DROITE, 0, MASTER_DROITE, 2);
AudioConnection          patchCord63(PERCU_DROITE, 0, MASTER_DROITE, 1);
AudioConnection          patchCord64(PERCU_GAUCHE, 0, MASTER_GAUCHE, 1);
AudioConnection          patchCord65(biquad1, 0, EFFET_DROITE, 2);
AudioConnection          patchCord66(biquad1, 0, EFFET_GAUCHE, 2);
AudioConnection          patchCord67(bitcrusher1, 0, EFFET_GAUCHE, 1);
AudioConnection          patchCord68(bitcrusher1, 0, EFFET_DROITE, 1);
AudioConnection          patchCord69(reverb1, 0, EFFET_GAUCHE, 0);
AudioConnection          patchCord70(reverb1, 0, EFFET_DROITE, 0);
AudioConnection          patchCord71(EFFET_DROITE, 0, MASTER_DROITE, 0);
AudioConnection          patchCord72(EFFET_GAUCHE, 0, MASTER_GAUCHE, 0);
AudioConnection          patchCord73(MASTER_DROITE, 0, i2s1, 1);
AudioConnection          patchCord74(MASTER_GAUCHE, 0, i2s1, 0);
