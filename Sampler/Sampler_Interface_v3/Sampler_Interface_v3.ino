#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <Routage_Teensy_Connections.h>
#include <Volume_Pano_Teensy.h>


#define SNARE

#ifdef SNARE
AudioSynthDrumHeart    snare;
AudioMixer4            snaremix;
AudioEffectMultiply    MUL_SNARE;
#endif


void setup() {
  // give the audio library some memory.  We'll be able
  // to see how much it actually uses, which can be used
  // to reduce this to the minimum necessary.
  AudioMemory(40);

  // enable the audio shield
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.6);

//////////////// CONSTANTES ///////////////////

  const int trig_drum1 = 30;
  const int trig_Hat = 29;
  const int trig_cymbal = 28;
  const int trig_drum4 = 27;

  const int trig_reverb = 26;
  const int trig_bitcrusher = 25;
  const int trig_biquad = 24;

  const int pano_drum1 = 37;
  const int pano_Hat = 38;
  const int pano_cymbal = 31;
  const int pano_drum4 = 32;
  const int vol_master = 39;

  const int length_hat_decay = 40;

  const int vol_drum1 = 36;
  const int vol_Hat = 35;
  const int vol_cymbal = 34;
  const int vol_drum4 = 33;

  // Modes des boutons en entrée pull_up
  pinMode(30, INPUT_PULLUP);
  pinMode(29, INPUT_PULLUP);
  pinMode(28, INPUT_PULLUP);
  pinMode(27, INPUT_PULLUP);
  pinMode(26, INPUT_PULLUP);
  pinMode(25, INPUT_PULLUP);
  pinMode(24, INPUT_PULLUP);

  //Modes des potentiometres en entrée
  
  pinMode(31, INPUT);
  pinMode(32, INPUT);
  pinMode(33, INPUT);
  pinMode(34, INPUT);
  pinMode(35, INPUT);
  pinMode(36, INPUT);
  pinMode(37, INPUT);
  pinMode(38, INPUT);
  pinMode(39, INPUT);
  pinMode(40,INPUT);

  drum1.frequency(380);
  drum1.length(30);
  //drum2.frequency(380);
  //drum2.length(30);
  //drum3.frequency(380);
  //drum3.length(30);
  drum4.frequency(380);
  drum4.length(30);
  //drum1.pitchMod(0,3);

  PERCU_ENVOI_EFFET1.gain(0, 1.0);
  PERCU_ENVOI_EFFET1.gain(1, 1.0);
  PERCU_ENVOI_EFFET1.gain(2, 1.0);
  PERCU_ENVOI_EFFET1.gain(3, 1.0);

  PERCU_ENVOI_EFFET2.gain(0, 1.0);
  PERCU_ENVOI_EFFET2.gain(1, 1.0);
  PERCU_ENVOI_EFFET2.gain(2, 1.0);
  PERCU_ENVOI_EFFET2.gain(3, 1.0);

  PERCU_ENVOI_EFFET3.gain(0, 1.0);
  PERCU_ENVOI_EFFET3.gain(1, 1.0);
  PERCU_ENVOI_EFFET3.gain(2, 1.0);
  PERCU_ENVOI_EFFET3.gain(3, 1.0);

  EFFET_GAUCHE.gain(0, 1.0);
  EFFET_DROITE.gain(0, 1.0);
  EFFET_GAUCHE.gain(1, 1.0);
  EFFET_DROITE.gain(1, 1.0);
  EFFET_GAUCHE.gain(2, 1.0);
  EFFET_DROITE.gain(2, 1.0);

  MASTER_GAUCHE.gain(0, 0.5);
  MASTER_GAUCHE.gain(1, 0.5);
  MASTER_DROITE.gain(0, 0.5);
  MASTER_DROITE.gain(1, 0.5);

  paramHat();
  paramCymbal();

  Serial.begin(58000);
}

void loop()
{
  // Lecture des valeurs des boutons de déclenchements de percus et effets
  int val_trig_drum1 = digitalRead(30);
  int val_trig_Hat = digitalRead(29);
  int val_trig_cymbal = digitalRead(28);
  int val_trig_drum4 = digitalRead(27);

  int val_trig_reverb = digitalRead(26);
  int val_trig_bitcrusher = digitalRead(25);
  int val_trig_biquad = digitalRead (24);

  if (!val_trig_drum1)
  {
    drum1.noteOn();
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_Hat)
  {
    DECAY_HAT.length(200);
    DECAY_HAT.noteOn(0x7fff);
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_cymbal)
  {
    DECAY_CYMBAL.noteOn(0x7fff);
    Serial.println("Cymbale declenchee");
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_drum4)
  {
    drum4.pitchMod(0.7);
    drum4.secondMix(2);
    //drum4.length(50);
    drum4.noteOn();
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }



  ///////////////////////////////////////////////////////////////////////////////////
  // Lecture analogique des potentiomètres pour déterminer volumes et panoramiques //
  ///////////////////////////////////////////////////////////////////////////////////

  int val_vol_drum1 = analogRead (36);
  int val_pano_drum1 = analogRead (37);
  Serial.print("Volume Drum1 : ");
  Serial.println(val_vol_drum1);
  Serial.print("Pano Drum1 : ");
  Serial.println(val_pano_drum1);

  int val_vol_Hat = analogRead (35);
  int val_pano_Hat = analogRead (38);

  int val_vol_cymbal = analogRead (34);
  int val_pano_cymbal = analogRead (31);

  int val_vol_drum4 = analogRead (33);
  int val_pano_drum4 = analogRead (32);

  int val_vol_master = analogRead (39);
  Serial.print("Volume Master : ");
  Serial.println(val_vol_master);

  ///////////////////////////////////////////////////////////////////////
  // appel aux fonctions de réglages des amplificateurs pour le volume //
  ///////////////////////////////////////////////////////////////////////

  volume_drum1(val_vol_drum1);
  volume_drum2(val_vol_Hat);
  volume_drum3(val_vol_cymbal);
  volume_drum4(val_vol_drum4);


  //////////////////////////////////////////////////////////////////////////
  //appel aux fonctions de réglage des mixers en fonction des panoramiques//
  //////////////////////////////////////////////////////////////////////////

  pano_drum(val_pano_drum1, val_pano_Hat, val_pano_cymbal, val_pano_drum4);
  vol_master(val_vol_master);
  delay(500);
}



///////////////
// FONCTIONS //
///////////////

/**
  * @fn void trig_reverb(int val_trig_reverb)
  * @brief Laisse passer les percus ou samples dans la reverberation si le bouton est pressé.
  * @param val_trig_reverb: variable renvoyant 0 si le bouton pour déclencher la reverb est pressé.
  */
void trig_reverb(int val_trig_reverb)
{
  // On laisse passer la reverberation tant que le bouton est pressé

  if (!val_trig_reverb)
  {
    MIX_ENVOI_EFFET1.gain(0, 1.0);
    MIX_ENVOI_EFFET1.gain(1, 1.0);
    MIX_ENVOI_EFFET1.gain(2, 1.0);
    MIX_ENVOI_EFFET1.gain(3, 1.0);
    reverb1.reverbTime(0.5);
  }
  else
  {
    MIX_ENVOI_EFFET1.gain(0, 0);
    MIX_ENVOI_EFFET1.gain(1, 0);
    MIX_ENVOI_EFFET1.gain(2, 0);
    MIX_ENVOI_EFFET1.gain(3, 0);
  }

}

/**
  * @fn void trig_bitcrusher(int val_trig_bitcrusher)
  * @brief Laisse passer les percus ou samples dans le bitcrusher si le bouton est pressé.
  * @param val_trig_bitcrusher: variable renvoyant 0 si le bouton pour déclencher le bitcrusher est pressé.
  */
void trig_bitcrusher(int val_trig_bitcrusher)
{
  // On laisse passer dans le bitcrusher tant que le bouton est pressé

  if (!val_trig_bitcrusher)
  {
    MIX_ENVOI_EFFET2.gain(0, 1.0);
    MIX_ENVOI_EFFET2.gain(1, 1.0);
    MIX_ENVOI_EFFET2.gain(2, 1.0);
    MIX_ENVOI_EFFET2.gain(3, 1.0);
    bitcrusher1.bits(6);
  }
  else
  {
    MIX_ENVOI_EFFET2.gain(0, 0);
    MIX_ENVOI_EFFET2.gain(1, 0);
    MIX_ENVOI_EFFET2.gain(2, 0);
    MIX_ENVOI_EFFET2.gain(3, 0);
  }

}


/**
  * @fn void trig_biquad(int val_trig_biquad)
  * @brief Laisse passer les percus ou samples dans le filtre biquad si le bouton est pressé.
  * @param val_trig_biquad: variable renvoyant 0 si le bouton pour déclencher le filtre est pressé.
  */

void trig_biquad(int val_trig_biquad)
{
  // On laisse passer dans le filtre si le bouton est pressé

  if (!val_trig_biquad)
  {
    MIX_ENVOI_EFFET3.gain(0, 1.0);
    MIX_ENVOI_EFFET3.gain(1, 1.0);
    MIX_ENVOI_EFFET3.gain(2, 1.0);
    MIX_ENVOI_EFFET3.gain(3, 1.0);
    biquad1.setLowpass(0, 6000, 0.3);
  }
  else
  {
    MIX_ENVOI_EFFET3.gain(0, 0);
    MIX_ENVOI_EFFET3.gain(1, 0);
    MIX_ENVOI_EFFET3.gain(2, 0);
    MIX_ENVOI_EFFET3.gain(3, 0);
  }

}

void paramCymbal()
{
    FILTER_CYMBAL.setLowpass(0, 4500, 0.3);
    FILTER_CYMBAL.setHighpass(1, 770, 0.7);
    DECAY_CYMBAL.length(1000);
}

void paramHat()
{
    FILTER_HAT.setHighpass(0, 700, 0.5);
    FILTER_HAT.setLowpass(1, 8000, 1.0);
}
