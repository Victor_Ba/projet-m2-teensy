#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioFilterBiquad        FILTER_CYMBAL;  //xy=1116,588.2499542236328
AudioFilterBiquad        FILTER_HAT;     //xy=1123,642.2499542236328
AudioPlaySdWav           playWav1;       //xy=1169.9999389648438,1176.25
AudioPlaySdWav           playWav2;       //xy=1169.9999389648438,1221.25
AudioPlaySdWav           playWav3;       //xy=1175.9999389648438,1272.25
AudioPlaySdWav           playWav4;       //xy=1187.9999389648438,1327.25
AudioSynthSimpleDrum     drum1;          //xy=1304,551.2499542236328
AudioSynthSimpleDrum     drum4;          //xy=1305,712.2499542236328
AudioEffectMultiply      MUL_CYMBAL;     //xy=1314,592.2499237060547
AudioEffectMultiply      MUL_HAT;        //xy=1315,646.2499542236328
AudioMixer4              MIX_WAV4;       //xy=1539.0000610351562,1533.2499694824219
AudioMixer4              MIX_WAV3;       //xy=1552.0000610351562,1448.25
AudioMixer4              MIX_WAV2;       //xy=1569.0000610351562,1374.25
AudioMixer4              MIX_WAV1;       //xy=1597.0000610351562,1301.2500305175781
AudioMixer4              PERCU_ENVOI_EFFET1; //xy=1605,777.2500152587891
AudioMixer4              PERCU_ENVOI_EFFET2; //xy=1610,850.2500152587891
AudioMixer4              PERCU_ENVOI_EFFET3; //xy=1612,926.2500152587891
AudioMixer4              SAMPLE_ENVOI_EFFET1; //xy=1614,1025.250015258789
AudioMixer4              SAMPLE_ENVOI_EFFET2; //xy=1617,1098.250015258789
AudioMixer4              SAMPLE_ENVOI_EFFET3; //xy=1620,1170.250015258789
AudioEffectGranular      granular1;      //xy=1759,1279.25
AudioEffectGranular      granular3;      //xy=1765,1375.25
AudioEffectGranular      granular2;      //xy=1776,1327.25
AudioEffectGranular      granular4;      //xy=1782,1423.25
AudioEffectEnvelope      envelope1;      //xy=1945,1278.25
AudioEffectEnvelope      envelope2;      //xy=1950,1326.25
AudioEffectEnvelope      envelope3;      //xy=1951,1374.25
AudioMixer4              MIX_ENVOI_EFFET1; //xy=1954,903.25
AudioEffectEnvelope      envelope4;      //xy=1956,1422.25
AudioMixer4              MIX_ENVOI_EFFET3; //xy=1958,1063.25
AudioMixer4              MIX_ENVOI_EFFET2; //xy=1961,986.25
AudioAmplifier           REGLAGE_VOLUME_MUL_CYMBAL; //xy=1977.9998168945312,576.2499847412109
AudioAmplifier           REGLAGE_VOLUME_DRUM1; //xy=1978.9998779296875,527.2499847412109
AudioAmplifier           REGLAGE_VOLUME_MUL_HAT; //xy=1982.9998168945312,628.2499847412109
AudioAmplifier           REGLAGE_VOLUME_DRUM4; //xy=1986.9998779296875,673.2499847412109
AudioFilterBiquad        biquad1;        //xy=2150,1093.25
AudioEffectBitcrusher    bitcrusher1;    //xy=2159,1024.25
AudioEffectReverb        reverb1;        //xy=2168,923.25
AudioMixer4              EFFET_DROITE;   //xy=2419,1079.25
AudioMixer4              EFFET_GAUCHE;   //xy=2421,989.25
AudioMixer4              PERCU_DROITE;   //xy=2518,730.25
AudioMixer4              PERCU_GAUCHE;   //xy=2520,640.25
AudioMixer4              SAMPLE_GAUCHE;  //xy=2519.0001220703125,1280.2500610351562
AudioMixer4              SAMPLE_DROITE;  //xy=2522.0001220703125,1364.2500610351562
AudioMixer4              MASTER_DROITE;  //xy=2892,1240.25
AudioMixer4              MASTER_GAUCHE;  //xy=2893,1166.25
AudioOutputI2S           i2s1;           //xy=3101,1200.25
AudioConnection          patchCord1(FILTER_CYMBAL, 0, MUL_CYMBAL, 0);
AudioConnection          patchCord2(FILTER_HAT, 0, MUL_HAT, 0);
AudioConnection          patchCord3(playWav1, 0, MIX_WAV1, 0);
AudioConnection          patchCord4(playWav1, 0, SAMPLE_ENVOI_EFFET1, 0);
AudioConnection          patchCord5(playWav1, 0, SAMPLE_ENVOI_EFFET2, 0);
AudioConnection          patchCord6(playWav1, 0, SAMPLE_ENVOI_EFFET3, 0);
AudioConnection          patchCord7(playWav1, 1, MIX_WAV1, 1);
AudioConnection          patchCord8(playWav2, 0, MIX_WAV2, 0);
AudioConnection          patchCord9(playWav2, 0, SAMPLE_ENVOI_EFFET1, 1);
AudioConnection          patchCord10(playWav2, 0, SAMPLE_ENVOI_EFFET2, 1);
AudioConnection          patchCord11(playWav2, 0, SAMPLE_ENVOI_EFFET3, 1);
AudioConnection          patchCord12(playWav2, 1, MIX_WAV2, 1);
AudioConnection          patchCord13(playWav3, 0, MIX_WAV3, 0);
AudioConnection          patchCord14(playWav3, 0, SAMPLE_ENVOI_EFFET1, 2);
AudioConnection          patchCord15(playWav3, 0, SAMPLE_ENVOI_EFFET2, 2);
AudioConnection          patchCord16(playWav3, 0, SAMPLE_ENVOI_EFFET3, 2);
AudioConnection          patchCord17(playWav4, 0, MIX_WAV4, 0);
AudioConnection          patchCord18(playWav4, 0, SAMPLE_ENVOI_EFFET1, 3);
AudioConnection          patchCord19(playWav4, 0, SAMPLE_ENVOI_EFFET2, 3);
AudioConnection          patchCord20(playWav4, 0, SAMPLE_ENVOI_EFFET3, 3);
AudioConnection          patchCord21(playWav4, 1, MIX_WAV3, 1);
AudioConnection          patchCord22(playWav4, 1, MIX_WAV4, 1);
AudioConnection          patchCord23(drum1, 0, PERCU_ENVOI_EFFET1, 0);
AudioConnection          patchCord24(drum1, 0, PERCU_ENVOI_EFFET2, 0);
AudioConnection          patchCord25(drum1, 0, PERCU_ENVOI_EFFET3, 0);
AudioConnection          patchCord26(drum1, REGLAGE_VOLUME_DRUM1);
AudioConnection          patchCord27(drum4, REGLAGE_VOLUME_DRUM4);
AudioConnection          patchCord28(drum4, 0, PERCU_ENVOI_EFFET1, 3);
AudioConnection          patchCord29(drum4, 0, PERCU_ENVOI_EFFET2, 3);
AudioConnection          patchCord30(drum4, 0, PERCU_ENVOI_EFFET3, 3);
AudioConnection          patchCord31(MUL_CYMBAL, REGLAGE_VOLUME_MUL_CYMBAL);
AudioConnection          patchCord32(MUL_CYMBAL, 0, PERCU_ENVOI_EFFET1, 2);
AudioConnection          patchCord33(MUL_CYMBAL, 0, PERCU_ENVOI_EFFET2, 2);
AudioConnection          patchCord34(MUL_HAT, REGLAGE_VOLUME_MUL_HAT);
AudioConnection          patchCord35(MUL_HAT, 0, PERCU_ENVOI_EFFET1, 1);
AudioConnection          patchCord36(MUL_HAT, 0, PERCU_ENVOI_EFFET2, 1);
AudioConnection          patchCord37(MUL_HAT, 0, PERCU_ENVOI_EFFET3, 1);
AudioConnection          patchCord38(MUL_HAT, 0, PERCU_ENVOI_EFFET3, 2);
AudioConnection          patchCord39(MIX_WAV4, granular4);
AudioConnection          patchCord40(MIX_WAV3, granular3);
AudioConnection          patchCord41(MIX_WAV2, granular2);
AudioConnection          patchCord42(MIX_WAV1, granular1);
AudioConnection          patchCord43(PERCU_ENVOI_EFFET1, 0, MIX_ENVOI_EFFET1, 0);
AudioConnection          patchCord44(PERCU_ENVOI_EFFET2, 0, MIX_ENVOI_EFFET2, 0);
AudioConnection          patchCord45(PERCU_ENVOI_EFFET3, 0, MIX_ENVOI_EFFET3, 0);
AudioConnection          patchCord46(SAMPLE_ENVOI_EFFET1, 0, MIX_ENVOI_EFFET1, 1);
AudioConnection          patchCord47(SAMPLE_ENVOI_EFFET2, 0, MIX_ENVOI_EFFET2, 1);
AudioConnection          patchCord48(SAMPLE_ENVOI_EFFET3, 0, MIX_ENVOI_EFFET3, 1);
AudioConnection          patchCord49(granular1, envelope1);
AudioConnection          patchCord50(granular3, envelope3);
AudioConnection          patchCord51(granular2, envelope2);
AudioConnection          patchCord52(granular4, envelope4);
AudioConnection          patchCord53(envelope1, 0, SAMPLE_GAUCHE, 0);
AudioConnection          patchCord54(envelope1, 0, SAMPLE_DROITE, 0);
AudioConnection          patchCord55(envelope2, 0, SAMPLE_GAUCHE, 1);
AudioConnection          patchCord56(envelope2, 0, SAMPLE_DROITE, 1);
AudioConnection          patchCord57(envelope3, 0, SAMPLE_GAUCHE, 2);
AudioConnection          patchCord58(envelope3, 0, SAMPLE_DROITE, 2);
AudioConnection          patchCord59(MIX_ENVOI_EFFET1, reverb1);
AudioConnection          patchCord60(envelope4, 0, SAMPLE_DROITE, 3);
AudioConnection          patchCord61(MIX_ENVOI_EFFET3, biquad1);
AudioConnection          patchCord62(MIX_ENVOI_EFFET2, bitcrusher1);
AudioConnection          patchCord63(REGLAGE_VOLUME_MUL_CYMBAL, 0, PERCU_DROITE, 2);
AudioConnection          patchCord64(REGLAGE_VOLUME_MUL_CYMBAL, 0, PERCU_GAUCHE, 2);
AudioConnection          patchCord65(REGLAGE_VOLUME_DRUM1, 0, PERCU_GAUCHE, 0);
AudioConnection          patchCord66(REGLAGE_VOLUME_DRUM1, 0, PERCU_DROITE, 0);
AudioConnection          patchCord67(REGLAGE_VOLUME_MUL_HAT, 0, PERCU_GAUCHE, 1);
AudioConnection          patchCord68(REGLAGE_VOLUME_MUL_HAT, 0, PERCU_DROITE, 1);
AudioConnection          patchCord69(REGLAGE_VOLUME_DRUM4, 0, PERCU_GAUCHE, 3);
AudioConnection          patchCord70(REGLAGE_VOLUME_DRUM4, 0, PERCU_DROITE, 3);
AudioConnection          patchCord71(biquad1, 0, EFFET_DROITE, 2);
AudioConnection          patchCord72(biquad1, 0, EFFET_GAUCHE, 2);
AudioConnection          patchCord73(bitcrusher1, 0, EFFET_GAUCHE, 1);
AudioConnection          patchCord74(bitcrusher1, 0, EFFET_DROITE, 1);
AudioConnection          patchCord75(reverb1, 0, EFFET_GAUCHE, 0);
AudioConnection          patchCord76(reverb1, 0, EFFET_DROITE, 0);
AudioConnection          patchCord77(EFFET_DROITE, 0, MASTER_DROITE, 0);
AudioConnection          patchCord78(EFFET_GAUCHE, 0, MASTER_GAUCHE, 0);
AudioConnection          patchCord79(PERCU_DROITE, 0, MASTER_DROITE, 1);
AudioConnection          patchCord80(PERCU_GAUCHE, 0, MASTER_GAUCHE, 1);
AudioConnection          patchCord81(SAMPLE_GAUCHE, 0, MASTER_GAUCHE, 2);
AudioConnection          patchCord82(SAMPLE_DROITE, 0, MASTER_DROITE, 2);
AudioConnection          patchCord83(MASTER_DROITE, 0, i2s1, 1);
AudioConnection          patchCord84(MASTER_GAUCHE, 0, i2s1, 0);
AudioControlSGTL5000     sgtl5000_1;     //xy=2991,979.2499389648438
// GUItool: end automatically generated code



void setup() {
  // give the audio library some memory.  We'll be able
  // to see how much it actually uses, which can be used
  // to reduce this to the minimum necessary.
  AudioMemory(40);

  // enable the audio shield
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.6);

  drum1.frequency(440);
  drum1.length(30);
  //drum1.pitchMod(0,3);

  REGLAGE_VOLUME_DRUM1.gain(0.5);
  PERCU_GAUCHE.gain(0, 0.5);
  PERCU_DROITE.gain(0, 0.5);
  
  PERCU_ENVOI_EFFET1.gain(0, 0.5);
  MIX_ENVOI_EFFET1.gain(0, 0.5);

  reverb1.reverbTime(0.3);

  EFFET_GAUCHE.gain(0, 0.5);
  EFFET_DROITE.gain(0, 0.5);

  MASTER_GAUCHE.gain(0, 0.5);
  MASTER_DROITE.gain(0, 0.5);

   
}

void loop() {
  drum1.noteOn();
  delay(1000);
}
