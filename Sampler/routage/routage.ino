#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioSynthSimpleDrum     drum2;          //xy=113,649
AudioSynthSimpleDrum     drum1;          //xy=114,585
AudioSynthSimpleDrum     drum4;          //xy=114,759
AudioSynthSimpleDrum     drum3;          //xy=115,701
AudioPlaySdRaw           playSdRaw1;     //xy=131,823
AudioPlaySdRaw           playSdRaw2;     //xy=131,868
AudioPlaySdRaw           playSdRaw3; //xy=137.75,919.75
AudioPlaySdRaw           playSdRaw4; //xy=137.75,964.75
AudioEffectGranular      granular1;      //xy=317,821
AudioMixer4              PERCU_ENVOI_EFFET1;         //xy=322,114
AudioEffectGranular      granular3; //xy=323.75,917.75
AudioMixer4              PERCU_ENVOI_EFFET2; //xy=327,187
AudioMixer4              PERCU_ENVOI_EFFET3; //xy=329,263
AudioMixer4              SAMPLE_ENVOI_EFFET1; //xy=331.75,362.75
AudioMixer4              SAMPLE_ENVOI_EFFET2; //xy=334.75,435.75
AudioEffectGranular      granular2;      //xy=334,869
AudioMixer4              SAMPLE_ENVOI_EFFET3; //xy=337.75,507.75
AudioEffectGranular      granular4; //xy=340.75,965.75
AudioAmplifier           REGLAGE_VOLUME_DRUM3; //xy=394,695
AudioAmplifier           REGLAGE_VOLUME_DRUM2; //xy=395,647
AudioAmplifier           REGLAGE_VOLUME_DRUM1; //xy=401,588
AudioAmplifier           REGLAGE_VOLUME_DRUM4; //xy=409,734
AudioEffectEnvelope      envelope1;      //xy=503,820
AudioEffectEnvelope      envelope2; //xy=508,868
AudioEffectEnvelope      envelope3; //xy=509.75,916.75
AudioEffectEnvelope      envelope4; //xy=514.75,964.75
AudioMixer4              MIX_ENVOI_EFFET1;         //xy=600.75,233.75
AudioMixer4              MIX_ENVOI_EFFET3; //xy=604.75,393.75
AudioMixer4              MIX_ENVOI_EFFET2; //xy=607.75,316.75
AudioMixer4              SAMPLE_GAUCHE;         //xy=708,798
AudioMixer4              SAMPLE_DROITE; //xy=711,882
AudioMixer4              PERCU_DROITE; //xy=719,694
AudioMixer4              PERCU_GAUCHE;         //xy=721,604
AudioFilterBiquad        biquad1;        //xy=796,423
AudioEffectBitcrusher    bitcrusher1;    //xy=805,354
AudioEffectReverb        reverb1;        //xy=814,253
AudioMixer4              EFFET_DROITE; //xy=1065,409
AudioMixer4              EFFET_GAUCHE; //xy=1067,319
AudioMixer4              MASTER_DROITE; //xy=1068,792
AudioMixer4              MASTER_GAUCHE;         //xy=1069,718
AudioOutputI2S           i2s1;           //xy=1277,752
AudioConnection          patchCord1(drum2, REGLAGE_VOLUME_DRUM2);
AudioConnection          patchCord2(drum2, 0, PERCU_ENVOI_EFFET1, 1);
AudioConnection          patchCord3(drum2, 0, PERCU_ENVOI_EFFET2, 1);
AudioConnection          patchCord4(drum2, 0, PERCU_ENVOI_EFFET3, 1);
AudioConnection          patchCord5(drum2, 0, PERCU_ENVOI_EFFET3, 2);
AudioConnection          patchCord6(drum1, 0, PERCU_ENVOI_EFFET1, 0);
AudioConnection          patchCord7(drum1, 0, PERCU_ENVOI_EFFET2, 0);
AudioConnection          patchCord8(drum1, 0, PERCU_ENVOI_EFFET3, 0);
AudioConnection          patchCord9(drum1, REGLAGE_VOLUME_DRUM1);
AudioConnection          patchCord10(drum4, REGLAGE_VOLUME_DRUM4);
AudioConnection          patchCord11(drum4, 0, PERCU_ENVOI_EFFET1, 3);
AudioConnection          patchCord12(drum4, 0, PERCU_ENVOI_EFFET2, 3);
AudioConnection          patchCord13(drum4, 0, PERCU_ENVOI_EFFET3, 3);
AudioConnection          patchCord14(drum3, REGLAGE_VOLUME_DRUM3);
AudioConnection          patchCord15(drum3, 0, PERCU_ENVOI_EFFET1, 2);
AudioConnection          patchCord16(drum3, 0, PERCU_ENVOI_EFFET2, 2);
AudioConnection          patchCord17(playSdRaw1, granular1);
AudioConnection          patchCord18(playSdRaw1, 0, SAMPLE_ENVOI_EFFET1, 0);
AudioConnection          patchCord19(playSdRaw1, 0, SAMPLE_ENVOI_EFFET2, 0);
AudioConnection          patchCord20(playSdRaw1, 0, SAMPLE_ENVOI_EFFET3, 0);
AudioConnection          patchCord21(playSdRaw2, granular2);
AudioConnection          patchCord22(playSdRaw2, 0, SAMPLE_ENVOI_EFFET1, 1);
AudioConnection          patchCord23(playSdRaw2, 0, SAMPLE_ENVOI_EFFET2, 1);
AudioConnection          patchCord24(playSdRaw2, 0, SAMPLE_ENVOI_EFFET3, 1);
AudioConnection          patchCord25(playSdRaw3, granular3);
AudioConnection          patchCord26(playSdRaw3, 0, SAMPLE_ENVOI_EFFET1, 2);
AudioConnection          patchCord27(playSdRaw3, 0, SAMPLE_ENVOI_EFFET2, 2);
AudioConnection          patchCord28(playSdRaw3, 0, SAMPLE_ENVOI_EFFET3, 2);
AudioConnection          patchCord29(playSdRaw4, granular4);
AudioConnection          patchCord30(playSdRaw4, 0, SAMPLE_ENVOI_EFFET1, 3);
AudioConnection          patchCord31(playSdRaw4, 0, SAMPLE_ENVOI_EFFET2, 3);
AudioConnection          patchCord32(playSdRaw4, 0, SAMPLE_ENVOI_EFFET3, 3);
AudioConnection          patchCord33(granular1, envelope1);
AudioConnection          patchCord34(PERCU_ENVOI_EFFET1, 0, MIX_ENVOI_EFFET1, 0);
AudioConnection          patchCord35(granular3, envelope3);
AudioConnection          patchCord36(PERCU_ENVOI_EFFET2, 0, MIX_ENVOI_EFFET2, 0);
AudioConnection          patchCord37(PERCU_ENVOI_EFFET3, 0, MIX_ENVOI_EFFET3, 0);
AudioConnection          patchCord38(SAMPLE_ENVOI_EFFET1, 0, MIX_ENVOI_EFFET1, 1);
AudioConnection          patchCord39(SAMPLE_ENVOI_EFFET2, 0, MIX_ENVOI_EFFET2, 1);
AudioConnection          patchCord40(granular2, envelope2);
AudioConnection          patchCord41(SAMPLE_ENVOI_EFFET3, 0, MIX_ENVOI_EFFET3, 1);
AudioConnection          patchCord42(granular4, envelope4);
AudioConnection          patchCord43(REGLAGE_VOLUME_DRUM3, 0, PERCU_DROITE, 2);
AudioConnection          patchCord44(REGLAGE_VOLUME_DRUM3, 0, PERCU_GAUCHE, 2);
AudioConnection          patchCord45(REGLAGE_VOLUME_DRUM2, 0, PERCU_GAUCHE, 1);
AudioConnection          patchCord46(REGLAGE_VOLUME_DRUM2, 0, PERCU_DROITE, 1);
AudioConnection          patchCord47(REGLAGE_VOLUME_DRUM1, 0, PERCU_GAUCHE, 0);
AudioConnection          patchCord48(REGLAGE_VOLUME_DRUM1, 0, PERCU_DROITE, 0);
AudioConnection          patchCord49(REGLAGE_VOLUME_DRUM4, 0, PERCU_GAUCHE, 3);
AudioConnection          patchCord50(REGLAGE_VOLUME_DRUM4, 0, PERCU_DROITE, 3);
AudioConnection          patchCord51(envelope1, 0, SAMPLE_GAUCHE, 0);
AudioConnection          patchCord52(envelope1, 0, SAMPLE_DROITE, 0);
AudioConnection          patchCord53(envelope2, 0, SAMPLE_GAUCHE, 1);
AudioConnection          patchCord54(envelope2, 0, SAMPLE_DROITE, 1);
AudioConnection          patchCord55(envelope3, 0, SAMPLE_GAUCHE, 2);
AudioConnection          patchCord56(envelope3, 0, SAMPLE_DROITE, 2);
AudioConnection          patchCord57(envelope4, 0, SAMPLE_DROITE, 3);
AudioConnection          patchCord58(MIX_ENVOI_EFFET1, reverb1);
AudioConnection          patchCord59(MIX_ENVOI_EFFET3, biquad1);
AudioConnection          patchCord60(MIX_ENVOI_EFFET2, bitcrusher1);
AudioConnection          patchCord61(SAMPLE_GAUCHE, 0, MASTER_GAUCHE, 2);
AudioConnection          patchCord62(SAMPLE_DROITE, 0, MASTER_DROITE, 2);
AudioConnection          patchCord63(PERCU_DROITE, 0, MASTER_DROITE, 1);
AudioConnection          patchCord64(PERCU_GAUCHE, 0, MASTER_GAUCHE, 1);
AudioConnection          patchCord65(biquad1, 0, EFFET_DROITE, 2);
AudioConnection          patchCord66(biquad1, 0, EFFET_GAUCHE, 2);
AudioConnection          patchCord67(bitcrusher1, 0, EFFET_GAUCHE, 1);
AudioConnection          patchCord68(bitcrusher1, 0, EFFET_DROITE, 1);
AudioConnection          patchCord69(reverb1, 0, EFFET_GAUCHE, 0);
AudioConnection          patchCord70(reverb1, 0, EFFET_DROITE, 0);
AudioConnection          patchCord71(EFFET_DROITE, 0, MASTER_DROITE, 0);
AudioConnection          patchCord72(EFFET_GAUCHE, 0, MASTER_GAUCHE, 0);
AudioConnection          patchCord73(MASTER_DROITE, 0, i2s1, 1);
AudioConnection          patchCord74(MASTER_GAUCHE, 0, i2s1, 0);
AudioControlSGTL5000     sgtl5000_1;     //xy=844,948
// GUItool: end automatically generated code


void setup() {
  // give the audio library some memory.  We'll be able
  // to see how much it actually uses, which can be used
  // to reduce this to the minimum necessary.
  AudioMemory(40);

  // enable the audio shield
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.6);

  drum1.frequency(440);
  drum1.length(30);
  //drum1.pitchMod(0,3);

  REGLAGE_VOLUME_DRUM1.gain(0.5);
  PERCU_GAUCHE.gain(0, 0.5);
  PERCU_DROITE.gain(0, 0.5);
  
  PERCU_ENVOI_EFFET1.gain(0, 0.5);
  MIX_ENVOI_EFFET1.gain(0, 0.5);

  reverb1.reverbTime(0.3);

  EFFET_GAUCHE.gain(0, 0.5);
  EFFET_DROITE.gain(0, 0.5);

  MASTER_GAUCHE.gain(0, 0.5);
  MASTER_DROITE.gain(0, 0.5);

   
}

void loop() {
  drum1.noteOn();
  delay(1000);
}
