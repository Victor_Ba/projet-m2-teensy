#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <Routage_Teensy_Connections.h>
#include <Volume_Pano_Teensy.h>

void setup() {
  // give the audio library some memory.  We'll be able
  // to see how much it actually uses, which can be used
  // to reduce this to the minimum necessary.
  AudioMemory(40);

  // enable the audio shield
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.6);

//////////////// CONSTANTES ///////////////////

  const int trig_drum1 = 30;
  const int trig_drum2 = 29;
  const int trig_drum3 = 28;
  const int trig_drum4 = 27;

  const int trig_reverb = 26;
  const int trig_bitcrusher = 25;
  const int trig_biquad = 24;

  const int pano_drum1 = 37;
  const int pano_drum2 = 38;
  const int pano_drum3 = 31;
  const int pano_drum4 = 32;
  const int pano_master = 39;

  const int vol_drum1 = 36;
  const int vol_drum2 = 35;
  const int vol_drum3 = 34;
  const int vol_drum4 = 33;

  // Modes des boutons en entrée pull_up
  pinMode(30, INPUT_PULLUP);
  pinMode(29, INPUT_PULLUP);
  pinMode(28, INPUT_PULLUP);
  pinMode(27, INPUT_PULLUP);
  pinMode(26, INPUT_PULLUP);
  pinMode(25, INPUT_PULLUP);
  pinMode(24, INPUT_PULLUP);

  //Modes des potentiometres en entrée
  pinMode(37, INPUT);
  pinMode(38, INPUT);
  pinMode(39, INPUT);
  pinMode(31, INPUT);
  pinMode(32, INPUT);
  pinMode(33, INPUT);
  pinMode(34, INPUT);
  pinMode(35, INPUT);
  pinMode(36, INPUT);


  drum1.frequency(380);
  drum1.length(30);
  drum2.frequency(380);
  drum2.length(30);
  drum3.frequency(380);
  drum3.length(30);
  drum4.frequency(380);
  drum4.length(30);
  //drum1.pitchMod(0,3);

  PERCU_ENVOI_EFFET1.gain(0, 1.0);
  PERCU_ENVOI_EFFET1.gain(1, 1.0);
  PERCU_ENVOI_EFFET1.gain(2, 1.0);
  PERCU_ENVOI_EFFET1.gain(3, 1.0);

  PERCU_ENVOI_EFFET2.gain(0, 1.0);
  PERCU_ENVOI_EFFET2.gain(1, 1.0);
  PERCU_ENVOI_EFFET2.gain(2, 1.0);
  PERCU_ENVOI_EFFET2.gain(3, 1.0);

  PERCU_ENVOI_EFFET3.gain(0, 1.0);
  PERCU_ENVOI_EFFET3.gain(1, 1.0);
  PERCU_ENVOI_EFFET3.gain(2, 1.0);
  PERCU_ENVOI_EFFET3.gain(3, 1.0);

  EFFET_GAUCHE.gain(0, 1.0);
  EFFET_DROITE.gain(0, 1.0);
  EFFET_GAUCHE.gain(1, 1.0);
  EFFET_DROITE.gain(1, 1.0);
  EFFET_GAUCHE.gain(2, 1.0);
  EFFET_DROITE.gain(2, 1.0);

  MASTER_GAUCHE.gain(0, 0.5);
  MASTER_GAUCHE.gain(1, 0.5);
  MASTER_DROITE.gain(0, 0.5);
  MASTER_DROITE.gain(1, 0.5);

  Serial.begin(58000);
}

void loop()
{

  int val_trig_drum1 = digitalRead(30);
  int val_trig_drum2 = digitalRead(29);
  int val_trig_drum3 = digitalRead(28);
  int val_trig_drum4 = digitalRead(27);

  int val_trig_reverb = digitalRead(26);
  int val_trig_bitcrusher = digitalRead(25);
  int val_trig_biquad = digitalRead (24);

  if (!val_trig_drum1)
  {
    drum1.noteOn();
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_drum2)
  {
    drum2.noteOn();
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_drum3)
  {
    drum3.noteOn();
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_drum4)
  {
    drum4.noteOn();
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }



  ///////////////////////////////////////////////////////////////////////////////////
  // Lecture analogique des potentiomètres pour déterminer volumes et panoramiques //
  ///////////////////////////////////////////////////////////////////////////////////

  int val_vol_drum1 = analogRead (36);
  int val_pano_drum1 = analogRead (37);
  Serial.print("Volume Drum1 : ");
  Serial.println(val_vol_drum1);
  Serial.print("Pano Drum1 : ");
  Serial.println(val_pano_drum1);

  int val_vol_drum2 = analogRead (35);
  int val_pano_drum2 = analogRead (38);

  int val_vol_drum3 = analogRead (34);
  int val_pano_drum3 = analogRead (31);

  int val_vol_drum4 = analogRead (33);
  int val_pano_drum4 = analogRead (32);

  int val_pano_master = analogRead (39);
  Serial.print("Pano Master : ");
  Serial.println(val_pano_master);

  ///////////////////////////////////////////////////////////////////////
  // appel aux fonctions de réglages des amplificateurs pour le volume //
  ///////////////////////////////////////////////////////////////////////

  volume_drum1(val_vol_drum1);
  volume_drum2(val_vol_drum2);
  volume_drum3(val_vol_drum3);
  volume_drum4(val_vol_drum4);


  //////////////////////////////////////////////////////////////////////////
  //appel aux fonctions de réglage des mixers en fonction des panoramiques//
  //////////////////////////////////////////////////////////////////////////

  pano_drum(val_pano_drum1, val_pano_drum2, val_pano_drum3, val_pano_drum4);
  pano_master(val_pano_master);
  delay(100);
}



///////////////
// FONCTIONS //
///////////////

/**
  * @fn void trig_reverb(int val_trig_reverb)
  * @brief Laisse passer les percus ou samples dans la reverberation si le bouton est pressé.
  * @param val_trig_reverb: variable renvoyant 0 si le bouton pour déclencher la reverb est pressé.
  */
void trig_reverb(int val_trig_reverb)
{
  // On laisse passer la reverberation tant que le bouton est pressé

  if (!val_trig_reverb)
  {
    MIX_ENVOI_EFFET1.gain(0, 1.0);
    MIX_ENVOI_EFFET1.gain(1, 1.0);
    MIX_ENVOI_EFFET1.gain(2, 1.0);
    MIX_ENVOI_EFFET1.gain(3, 1.0);
    reverb1.reverbTime(0.5);
  }
  else
  {
    MIX_ENVOI_EFFET1.gain(0, 0);
    MIX_ENVOI_EFFET1.gain(1, 0);
    MIX_ENVOI_EFFET1.gain(2, 0);
    MIX_ENVOI_EFFET1.gain(3, 0);
  }

}

/**
  * @fn void trig_bitcrusher(int val_trig_bitcrusher)
  * @brief Laisse passer les percus ou samples dans le bitcrusher si le bouton est pressé.
  * @param val_trig_bitcrusher: variable renvoyant 0 si le bouton pour déclencher le bitcrusher est pressé.
  */
void trig_bitcrusher(int val_trig_bitcrusher)
{
  // On laisse passer dans le bitcrusher tant que le bouton est pressé

  if (!val_trig_bitcrusher)
  {
    MIX_ENVOI_EFFET2.gain(0, 1.0);
    MIX_ENVOI_EFFET2.gain(1, 1.0);
    MIX_ENVOI_EFFET2.gain(2, 1.0);
    MIX_ENVOI_EFFET2.gain(3, 1.0);
    bitcrusher1.bits(4);
  }
  else
  {
    MIX_ENVOI_EFFET2.gain(0, 0);
    MIX_ENVOI_EFFET2.gain(1, 0);
    MIX_ENVOI_EFFET2.gain(2, 0);
    MIX_ENVOI_EFFET2.gain(3, 0);
  }

}


/**
  * @fn void trig_biquad(int val_trig_biquad)
  * @brief Laisse passer les percus ou samples dans le filtre biquad si le bouton est pressé.
  * @param val_trig_biquad: variable renvoyant 0 si le bouton pour déclencher le filtre est pressé.
  */

void trig_biquad(int val_trig_biquad)
{
  // On laisse passer dans le filtre si le bouton est pressé

  if (!val_trig_biquad)
  {
    MIX_ENVOI_EFFET3.gain(0, 1.0);
    MIX_ENVOI_EFFET3.gain(1, 1.0);
    MIX_ENVOI_EFFET3.gain(2, 1.0);
    MIX_ENVOI_EFFET3.gain(3, 1.0);
    biquad1.setLowpass(0, 2000, 0.3);
  }
  else
  {
    MIX_ENVOI_EFFET3.gain(0, 0);
    MIX_ENVOI_EFFET3.gain(1, 0);
    MIX_ENVOI_EFFET3.gain(2, 0);
    MIX_ENVOI_EFFET3.gain(3, 0);
  }

}
