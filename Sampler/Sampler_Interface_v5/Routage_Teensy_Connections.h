#include <Wire.h>
#include <Audio.h>
#include "Synth-Clatter.h"
#include "Synth-Decay.h"
#include "Synth-DrumHeart.h"


// GUItool: begin automatically generated code
AudioSynthClatter         clat1;
AudioSynthDecay          DECAY_HAT;
AudioSynthDecay          DECAY_CYMBAL;
AudioFilterBiquad        FILTER_CYMBAL;  //xy=195.00000762939453,168.03121948242188
AudioFilterBiquad        FILTER_HAT;     //xy=206.01255798339844,255.0312213897705
AudioSynthSimpleDrum     drum1;          //xy=298.01251220703125,641.0312538146973
AudioSynthSimpleDrum     drum4;          //xy=299.01250076293945,773.0312461853027
AudioPlayMemory    		 sample1;
AudioPlaySdWav           playWav2;       //xy=315.01251220703125,924.0312538146973
AudioPlaySdWav           playWav3;       //xy=321.01251220703125,975.0312538146973
AudioPlaySdWav           playWav4;       //xy=321.01251220703125,1020.0312538146973
AudioPlaySdWav           playWav1;       //xy=322.012508392334,877.0312080383301
AudioEffectMultiply      MUL_CYMBAL;     //xy=417.01253509521484,175.0312271118164
AudioEffectMultiply      MUL_HAT;        //xy=424.01253509521484,262.0312099456787
AudioMixer4              PERCU_ENVOI_EFFET1; //xy=799.0125274658203,178.03125381469727
AudioMixer4              PERCU_ENVOI_EFFET2; //xy=804.0125274658203,251.03125381469727
AudioMixer4              PERCU_ENVOI_EFFET3; //xy=806.0125274658203,327.03125381469727
AudioMixer4              SAMPLE_ENVOI_EFFET1; //xy=808.0125274658203,426.03125381469727
AudioMixer4              SAMPLE_ENVOI_EFFET2; //xy=811.0125274658203,499.03125381469727
AudioMixer4              SAMPLE_ENVOI_EFFET3; //xy=814.0125274658203,571.0312538146973
AudioAmplifier           REGLAGE_VOLUME_MUL_CYMBAL; //xy=871.0125274658203,759.0312538146973
AudioAmplifier           REGLAGE_VOLUME_MUL_HAT; //xy=872.0125274658203,711.0312538146973
AudioAmplifier           REGLAGE_VOLUME_DRUM1; //xy=878.0125274658203,652.0312538146973
AudioAmplifier           REGLAGE_VOLUME_DRUM4; //xy=886.0125274658203,798.0312538146973
AudioMixer4              MIX_ENVOI_EFFET1; //xy=1077.0125274658203,297.03125381469727
AudioMixer4              MIX_ENVOI_EFFET3; //xy=1081.0125274658203,457.03125381469727
AudioMixer4              MIX_ENVOI_EFFET2; //xy=1084.0125274658203,380.03125381469727
AudioMixer4              SAMPLE_GAUCHE;  //xy=1185.0125274658203,862.0312538146973
AudioMixer4              SAMPLE_DROITE;  //xy=1188.0125274658203,946.0312538146973
AudioMixer4              PERCU_DROITE;   //xy=1196.0125274658203,758.0312538146973
AudioMixer4              PERCU_GAUCHE;   //xy=1198.0125274658203,668.0312538146973
AudioFilterBiquad        biquad1;        //xy=1273.0125274658203,487.03125381469727
AudioEffectBitcrusher    bitcrusher1;    //xy=1282.0125274658203,418.03125381469727
AudioEffectReverb        reverb1;        //xy=1291.0125274658203,317.03125381469727
AudioMixer4              EFFET_DROITE;   //xy=1542.0125274658203,473.03125381469727
AudioMixer4              EFFET_GAUCHE;   //xy=1544.0125274658203,383.03125381469727
AudioMixer4              MASTER_DROITE;  //xy=1545.0125274658203,856.0312538146973
AudioMixer4              MASTER_GAUCHE;  //xy=1546.0125274658203,782.0312538146973
AudioOutputI2S           i2s1;           //xy=1754.0125274658203,816.0312538146973

AudioConnection       	 patchCord75(clat1, 0 ,FILTER_HAT,0);
AudioConnection       	 patchCord77(DECAY_HAT, 0 ,MUL_HAT,1);
AudioConnection       	 patchCord78(clat1, 0 ,FILTER_CYMBAL,0);
AudioConnection       	 patchCord80(DECAY_CYMBAL, 0 ,MUL_CYMBAL,1);

AudioConnection          patchCord1(FILTER_CYMBAL, 0, MUL_CYMBAL, 0);
AudioConnection          patchCord2(FILTER_HAT, 0, MUL_HAT, 0);
AudioConnection          patchCord3(drum1, 0, PERCU_ENVOI_EFFET1, 0);
AudioConnection          patchCord4(drum1, 0, PERCU_ENVOI_EFFET2, 0);
AudioConnection          patchCord5(drum1, 0, PERCU_ENVOI_EFFET3, 0);
AudioConnection          patchCord6(drum1, REGLAGE_VOLUME_DRUM1);
AudioConnection          patchCord7(drum4, REGLAGE_VOLUME_DRUM4);
AudioConnection          patchCord8(drum4, 0, PERCU_ENVOI_EFFET1, 3);
AudioConnection          patchCord9(drum4, 0, PERCU_ENVOI_EFFET2, 3);
AudioConnection          patchCord10(drum4, 0, PERCU_ENVOI_EFFET3, 3);
AudioConnection          patchCord11(playWav2, 0, SAMPLE_ENVOI_EFFET1, 1);
AudioConnection          patchCord12(playWav2, 0, SAMPLE_ENVOI_EFFET2, 1);
AudioConnection          patchCord13(playWav2, 0, SAMPLE_ENVOI_EFFET3, 1);
AudioConnection          patchCord14(playWav2, 0, SAMPLE_GAUCHE, 1);
AudioConnection          patchCord15(playWav2, 1, SAMPLE_DROITE, 1);
AudioConnection          patchCord16(playWav3, 0, SAMPLE_ENVOI_EFFET1, 2);
AudioConnection          patchCord17(playWav3, 0, SAMPLE_ENVOI_EFFET2, 2);
AudioConnection          patchCord18(playWav3, 0, SAMPLE_ENVOI_EFFET3, 2);
AudioConnection          patchCord19(playWav3, 0, SAMPLE_GAUCHE, 2);
AudioConnection          patchCord20(playWav3, 1, SAMPLE_DROITE, 2);
AudioConnection          patchCord21(playWav4, 0, SAMPLE_ENVOI_EFFET1, 3);
AudioConnection          patchCord22(playWav4, 0, SAMPLE_ENVOI_EFFET2, 3);
AudioConnection          patchCord23(playWav4, 0, SAMPLE_ENVOI_EFFET3, 3);
AudioConnection          patchCord24(playWav4, 0, SAMPLE_GAUCHE, 3);
AudioConnection          patchCord25(playWav4, 1, SAMPLE_DROITE, 3);
AudioConnection          patchCord26(sample1, 0, SAMPLE_ENVOI_EFFET1, 0);
AudioConnection          patchCord27(sample1, 0, SAMPLE_ENVOI_EFFET2, 0);
AudioConnection          patchCord28(sample1, 0, SAMPLE_ENVOI_EFFET3, 0);
AudioConnection          patchCord29(sample1, 0, SAMPLE_GAUCHE, 0);
AudioConnection          patchCord30(sample1, 1, SAMPLE_DROITE, 0);
AudioConnection          patchCord31(MUL_CYMBAL, REGLAGE_VOLUME_MUL_CYMBAL);
AudioConnection          patchCord32(MUL_CYMBAL, 0, PERCU_ENVOI_EFFET1, 2);
AudioConnection          patchCord33(MUL_CYMBAL, 0, PERCU_ENVOI_EFFET2, 2);
AudioConnection          patchCord34(MUL_HAT, REGLAGE_VOLUME_MUL_HAT);
AudioConnection          patchCord35(MUL_HAT, 0, PERCU_ENVOI_EFFET1, 1);
AudioConnection          patchCord36(MUL_HAT, 0, PERCU_ENVOI_EFFET2, 1);
AudioConnection          patchCord37(MUL_HAT, 0, PERCU_ENVOI_EFFET3, 1);
AudioConnection          patchCord38(MUL_HAT, 0, PERCU_ENVOI_EFFET3, 2);
AudioConnection          patchCord39(PERCU_ENVOI_EFFET1, 0, MIX_ENVOI_EFFET1, 0);
AudioConnection          patchCord40(PERCU_ENVOI_EFFET2, 0, MIX_ENVOI_EFFET2, 0);
AudioConnection          patchCord41(PERCU_ENVOI_EFFET3, 0, MIX_ENVOI_EFFET3, 0);
AudioConnection          patchCord42(SAMPLE_ENVOI_EFFET1, 0, MIX_ENVOI_EFFET1, 1);
AudioConnection          patchCord43(SAMPLE_ENVOI_EFFET2, 0, MIX_ENVOI_EFFET2, 1);
AudioConnection          patchCord44(SAMPLE_ENVOI_EFFET3, 0, MIX_ENVOI_EFFET3, 1);
AudioConnection          patchCord45(REGLAGE_VOLUME_MUL_CYMBAL, 0, PERCU_DROITE, 2);
AudioConnection          patchCord46(REGLAGE_VOLUME_MUL_CYMBAL, 0, PERCU_GAUCHE, 2);
AudioConnection          patchCord47(REGLAGE_VOLUME_MUL_HAT, 0, PERCU_GAUCHE, 1);
AudioConnection          patchCord48(REGLAGE_VOLUME_MUL_HAT, 0, PERCU_DROITE, 1);
AudioConnection          patchCord49(REGLAGE_VOLUME_DRUM1, 0, PERCU_GAUCHE, 0);
AudioConnection          patchCord50(REGLAGE_VOLUME_DRUM1, 0, PERCU_DROITE, 0);
AudioConnection          patchCord51(REGLAGE_VOLUME_DRUM4, 0, PERCU_GAUCHE, 3);
AudioConnection          patchCord52(REGLAGE_VOLUME_DRUM4, 0, PERCU_DROITE, 3);
AudioConnection          patchCord53(MIX_ENVOI_EFFET1, reverb1);
AudioConnection          patchCord54(MIX_ENVOI_EFFET3, biquad1);
AudioConnection          patchCord55(MIX_ENVOI_EFFET2, bitcrusher1);
AudioConnection          patchCord56(SAMPLE_GAUCHE, 0, MASTER_GAUCHE, 2);
AudioConnection          patchCord57(SAMPLE_DROITE, 0, MASTER_DROITE, 2);
AudioConnection          patchCord58(PERCU_DROITE, 0, MASTER_DROITE, 1);
AudioConnection          patchCord59(PERCU_GAUCHE, 0, MASTER_GAUCHE, 1);
AudioConnection          patchCord60(biquad1, 0, EFFET_DROITE, 2);
AudioConnection          patchCord61(biquad1, 0, EFFET_GAUCHE, 2);
AudioConnection          patchCord62(bitcrusher1, 0, EFFET_GAUCHE, 1);
AudioConnection          patchCord63(bitcrusher1, 0, EFFET_DROITE, 1);
AudioConnection          patchCord64(reverb1, 0, EFFET_GAUCHE, 0);
AudioConnection          patchCord65(reverb1, 0, EFFET_DROITE, 0);
AudioConnection          patchCord66(EFFET_DROITE, 0, MASTER_DROITE, 0);
AudioConnection          patchCord67(EFFET_GAUCHE, 0, MASTER_GAUCHE, 0);
AudioConnection          patchCord68(MASTER_DROITE, 0, i2s1, 1);
AudioConnection          patchCord69(MASTER_GAUCHE, 0, i2s1, 0);
AudioControlSGTL5000     sgtl5000_1;     //xy=1321.0125274658203,1012.0312538146973
// GUItool: end automatically generated code
