#ifndef synth_clatter_h_
#define synth_clatter_h_
#include "AudioStream.h"
#include "utility/dspinst.h"

//#define SAMPLES_PER_MSEC (AUDIO_SAMPLE_RATE_EXACT/1000.0)

class AudioSynthClatter : public AudioStream
{
public:

  AudioSynthClatter() : AudioStream(2, inputQueueArray) 
  {
    next_trip[0] = half_waves[0] = (AUDIO_SAMPLE_RATE_EXACT/(800*2));
    next_trip[1] = half_waves[1] = (AUDIO_SAMPLE_RATE_EXACT/(540*2));
    next_trip[2] = half_waves[2] = (AUDIO_SAMPLE_RATE_EXACT/(522*2));
    next_trip[3] = half_waves[3] = (AUDIO_SAMPLE_RATE_EXACT/(304*2));
    next_trip[4] = half_waves[4] = (AUDIO_SAMPLE_RATE_EXACT/(369*2));
    next_trip[5] = half_waves[5] = (AUDIO_SAMPLE_RATE_EXACT/(205*2));
  }

  using AudioStream::release;
  virtual void update(void);

  // public for debug...
  uint32_t count;
  uint32_t next_trip[6];  
  uint32_t values[6];

private:
  audio_block_t *inputQueueArray[2];

  // Denominators are values calculated from CYNBAL values
  uint32_t half_waves[6];// = {110,125,140,155,189,202};
                                            
};

#endif