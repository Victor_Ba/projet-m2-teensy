///////////////
// FONCTIONS //
///////////////

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include "Routage_Teensy_Connections.h"
#include "Volume_Pano_Teensy.h"
#include <MIDI.h>
#include "Variables.h"

/**
    @fn void init_SD()
    @brief initialise la liaison avec la carte SD.
*/
void init_SD()
{
  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);
  if (!(SD.begin(SDCARD_CS_PIN))) {
    // stop here, but print a message repetitively
    while (1) {
      Serial.println("Unable to access the SD card");
      delay(500);
    }
  }
}

/**
    @fn void init_Mixers()
    @brief initialise les gains des mixers.
*/
void init_Mixers()
{
  PERCU_ENVOI_EFFET1.gain(0, 1.0);
  PERCU_ENVOI_EFFET1.gain(1, 1.0);
  PERCU_ENVOI_EFFET1.gain(2, 1.0);
  PERCU_ENVOI_EFFET1.gain(3, 1.0);

  PERCU_ENVOI_EFFET2.gain(0, 1.0);
  PERCU_ENVOI_EFFET2.gain(1, 1.0);
  PERCU_ENVOI_EFFET2.gain(2, 1.0);
  PERCU_ENVOI_EFFET2.gain(3, 1.0);

  PERCU_ENVOI_EFFET3.gain(0, 1.0);
  PERCU_ENVOI_EFFET3.gain(1, 1.0);
  PERCU_ENVOI_EFFET3.gain(2, 1.0);
  PERCU_ENVOI_EFFET3.gain(3, 1.0);

  EFFET_GAUCHE.gain(0, 1.0);
  EFFET_DROITE.gain(0, 1.0);
  EFFET_GAUCHE.gain(1, 1.0);
  EFFET_DROITE.gain(1, 1.0);
  EFFET_GAUCHE.gain(2, 1.0);
  EFFET_DROITE.gain(2, 1.0);

  MASTER_GAUCHE.gain(0, 0.5);
  MASTER_GAUCHE.gain(1, 0.5);
  MASTER_DROITE.gain(0, 0.5);
  MASTER_DROITE.gain(1, 0.5);

  // Partie Samples
  SAMPLE_GAUCHE.gain(0,1.0);
  SAMPLE_GAUCHE.gain(1,1.0);
  SAMPLE_GAUCHE.gain(2,1.0);
  SAMPLE_GAUCHE.gain(3,1.0);
  
  SAMPLE_DROITE.gain(0,1.0);
  SAMPLE_DROITE.gain(1,1.0);
  SAMPLE_DROITE.gain(2,1.0);
  SAMPLE_DROITE.gain(3,1.0);
  
  MASTER_GAUCHE.gain(2,1.0);
  MASTER_DROITE.gain(2,1.0);
}

/**
    @fn void init_Entrees_Sorties()
    @brief initialise les modes en entrées/sorties des potentiomètres ou boutons.
*/
void init_Entrees_Sorties()
{
  // Modes des boutons en entrée pull_up
  pinMode(trig_drum1, INPUT_PULLUP);
  pinMode(trig_Hat, INPUT_PULLUP);
  pinMode(trig_cymbal, INPUT_PULLUP);
  pinMode(trig_drum4, INPUT_PULLUP);
  pinMode(trigger_reverb, INPUT_PULLUP);
  pinMode(trigger_bitcrusher, INPUT_PULLUP);
  pinMode(trigger_biquad, INPUT_PULLUP);

  //Modes des potentiometres en entrée

  pinMode(pano_cymbal, INPUT);
  pinMode(pano_drum4, INPUT);
  pinMode(vol_drum4, INPUT);
  pinMode(vol_cymbal, INPUT);
  pinMode(vol_Hat, INPUT);
  pinMode(vol_drum1, INPUT);
  pinMode(pano_drum1, INPUT);
  pinMode(pano_Hat, INPUT);
  pinMode(volume_master, INPUT);
}

/**
    @fn void lecture_Boutons()
    @brief lit les valeurs des boutons pour déclencher les percus et effets.
*/
void lecture_Boutons()
{
  val_trig_drum1 = digitalRead(trig_drum1);
  val_trig_Hat = digitalRead(trig_Hat);
  val_trig_cymbal = digitalRead(trig_cymbal);
  val_trig_drum4 = digitalRead(trig_drum4);

  val_trig_reverb = digitalRead(trigger_reverb);
  val_trig_bitcrusher = digitalRead(trigger_bitcrusher);
  val_trig_biquad = digitalRead (trigger_biquad);
}

/**
    @fn void lecture_Pot()
    @brief lit les valeurs des potentiomètres pour déterminer volumes et panoramiques.
*/
void lecture_Pot()
{
  val_vol_drum1 = analogRead (vol_drum1);
  val_pano_drum1 = analogRead (pano_drum1);

  val_vol_Hat = analogRead (vol_Hat);
  val_pano_Hat = analogRead (pano_Hat);

  val_vol_cymbal = analogRead (vol_cymbal);
  val_pano_cymbal = analogRead (pano_cymbal);

  val_vol_drum4 = analogRead (vol_drum4);
  val_pano_drum4 = analogRead (pano_drum4);

  val_vol_master = analogRead (volume_master);
}

/**
    @fn void trig_reverb(int val_trig_reverb)
    @brief Laisse passer les percus ou samples dans la reverberation si le bouton est pressé.
    @param val_trig_reverb: variable renvoyant 0 si le bouton pour déclencher la reverb est pressé.
*/
void trig_reverb(int val_trig_reverb)
{
  // On laisse passer la reverberation tant que le bouton est pressé

  if (!val_trig_reverb)
  {
    MIX_ENVOI_EFFET1.gain(0, 1.0);
    MIX_ENVOI_EFFET1.gain(1, 1.0);
    MIX_ENVOI_EFFET1.gain(2, 1.0);
    MIX_ENVOI_EFFET1.gain(3, 1.0);
    reverb1.reverbTime(0.5);
  }
  else
  {
    MIX_ENVOI_EFFET1.gain(0, 0);
    MIX_ENVOI_EFFET1.gain(1, 0);
    MIX_ENVOI_EFFET1.gain(2, 0);
    MIX_ENVOI_EFFET1.gain(3, 0);
  }

}

/**
    @fn void trig_bitcrusher(int val_trig_bitcrusher)
    @brief Laisse passer les percus ou samples dans le bitcrusher si le bouton est pressé.
    @param val_trig_bitcrusher: variable renvoyant 0 si le bouton pour déclencher le bitcrusher est pressé.
*/
void trig_bitcrusher(int val_trig_bitcrusher)
{
  // On laisse passer dans le bitcrusher tant que le bouton est pressé

  if (!val_trig_bitcrusher)
  {
    MIX_ENVOI_EFFET2.gain(0, 1.0);
    MIX_ENVOI_EFFET2.gain(1, 1.0);
    MIX_ENVOI_EFFET2.gain(2, 1.0);
    MIX_ENVOI_EFFET2.gain(3, 1.0);
    bitcrusher1.bits(6);
  }
  else
  {
    MIX_ENVOI_EFFET2.gain(0, 0);
    MIX_ENVOI_EFFET2.gain(1, 0);
    MIX_ENVOI_EFFET2.gain(2, 0);
    MIX_ENVOI_EFFET2.gain(3, 0);
  }

}


/**
    @fn void trig_biquad(int val_trig_biquad)
    @brief Laisse passer les percus ou samples dans le filtre biquad si le bouton est pressé.
    @param val_trig_biquad: variable renvoyant 0 si le bouton pour déclencher le filtre est pressé.
*/

void trig_biquad(int val_trig_biquad)
{
  // On laisse passer dans le filtre si le bouton est pressé

  if (!val_trig_biquad)
  {
    MIX_ENVOI_EFFET3.gain(0, 1.0);
    MIX_ENVOI_EFFET3.gain(1, 1.0);
    MIX_ENVOI_EFFET3.gain(2, 1.0);
    MIX_ENVOI_EFFET3.gain(3, 1.0);
    biquad1.setLowpass(0, 6000, 0.3);
  }
  else
  {
    MIX_ENVOI_EFFET3.gain(0, 0);
    MIX_ENVOI_EFFET3.gain(1, 0);
    MIX_ENVOI_EFFET3.gain(2, 0);
    MIX_ENVOI_EFFET3.gain(3, 0);
  }

}

/**
    @fn void paramCymbal()
    @brief initialise les paramètres du filtre permettant de générer la cymbale.
*/
void paramCymbal()
{
  FILTER_CYMBAL.setLowpass(0, 4500, 0.3);
  FILTER_CYMBAL.setHighpass(1, 770, 0.7);
  DECAY_CYMBAL.length(1000);
}

/**
    @fn void paramCymbal()
    @brief initialise les paramètres du filtre permettant de générer le Hi-Hat.
*/
void paramHat()
{
  FILTER_HAT.setHighpass(0, 700, 0.5);
  FILTER_HAT.setLowpass(1, 8000, 1.0);
}