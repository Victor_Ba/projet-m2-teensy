#ifndef synth_decay_h_
#define synth_decay_h_
#include "AudioStream.h"
#include "utility/dspinst.h"
//#define SAMPLES_PER_MSEC (AUDIO_SAMPLE_RATE_EXACT/1000.0)
class AudioSynthDecay : public AudioStream
{
public:
  AudioSynthDecay() : AudioStream(0, NULL) // 0 for No inputs
  {
    env_lin_current = 0;
    env_sqr_current = 0;
    //length(1000);
  }
  void noteOn();
  void noteOn(int16_t topval = 0x7fff);

  void length(int32_t milliseconds)
  {
    //Serial.println("decay length");
    
    if(milliseconds < 0)
      return;
    else if(milliseconds == 0) // avoid div by 0.
      milliseconds = 1;
    else if(milliseconds > 5000) // arbitrary limit...
      milliseconds = 5000;
    int32_t len_samples = milliseconds*(AUDIO_SAMPLE_RATE_EXACT/1000.0);
//    __disable_irq();    
    env_decrement = (0x7fff0000/len_samples);
    
//    __enable_irq();    
  };
  using AudioStream::release;
  virtual void update(void);
  // public for debug...
  // Envelope params
  int32_t env_lin_current; // present value of linear slope.
  int32_t env_sqr_current; // the square of the linear value - quasi exponential..
  int32_t env_decrement;   // how each sample deviates from previous.
private:
  //audio_block_t *inputQueueArray[1];
};
#undef SAMPLES_PER_MSEC
#endif