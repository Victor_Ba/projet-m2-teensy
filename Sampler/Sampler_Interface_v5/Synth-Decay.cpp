#include "Synth-Decay.h"


void AudioSynthDecay::noteOn(int16_t topval)
{
  //Serial.println("Decay noteOn");
//  __disable_irq();

  env_lin_current = (topval << 16);
  
//  __enable_irq();
}

void AudioSynthDecay::update(void)
{
  //Serial.println("Decay update");
  
  audio_block_t *block_env;
  int16_t *p_env, *end;

  block_env = allocate();
  if (!block_env) return;
  p_env = (block_env->data);
  end = p_env + AUDIO_BLOCK_SAMPLES;

  while(p_env < end)
  {
    // Do envelope first
    if(env_lin_current < 0x0000ffff)
    {
      *p_env = 0;
    }
    else
    {
      env_lin_current -= env_decrement;
      env_sqr_current = multiply_16tx16t(env_lin_current, env_lin_current) ;
      *p_env = env_sqr_current>>15;
    } 

    p_env++;
  }

  transmit(block_env);
  release(block_env);
}
