#include "Fonctions.h"
#include "AudioSampleSnaredrum.h"   

void setup() {
  // give the audio library some memory.  We'll be able
  // to see how much it actually uses, which can be used
  // to reduce this to the minimum necessary.
  AudioMemory(40);

  // enable the audio shield
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.6);

  init_Entrees_Sorties();

  drum1.frequency(110);
  drum1.length(150);
  drum4.frequency(380);
  drum4.length(30);

  init_Mixers();

  // Initialisation des paramètres de cymbal et hat (decay, filtres...)
  paramHat();
  paramCymbal();

  Serial.begin(58000);
  MIDI.begin(MIDI_CHANNEL_OMNI);

  //Initialisation de la liaison avec la carte SD
  //init_SD();
}

void loop()
{
  // Lecture des valeurs des boutons de déclenchements de percus et effets
  lecture_Boutons();

  //Lecture des messages MIDI
  if (MIDI.read())
  {
    typeMidi = MIDI.getType();
    if (typeMidi == midi::NoteOn)
    {
      canalMidi = MIDI.getChannel();
      byte velocity = MIDI.getData2();
      switch(canalMidi)
      {
        case 1: 
          if(velocity>0){
            drum1.noteOn();
          }
          break;
        case 2: 
          if(velocity>0){
            DECAY_HAT.length(200);
            DECAY_HAT.noteOn(0x7fff);
          }
          break;
        case 3: 
          if(velocity>0){
            DECAY_CYMBAL.noteOn(0x7fff);
          }
          break;
        case 4: 
          if(velocity>0){
            drum4.noteOn();
          }
          break;
        case 5: 
          if(velocity>0){
            sample1.play(AudioSampleSnaredrum);
          }
          break;
      }
    }
  }

  if (!val_trig_drum1)
  {
    sample1.play(AudioSampleSnaredrum);
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_Hat)
  {
    DECAY_HAT.length(200);
    DECAY_HAT.noteOn(0x7fff);
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_cymbal)
  {
    DECAY_CYMBAL.noteOn(0x7fff);
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  if (!val_trig_drum4)
  {
    drum4.pitchMod(0.7);
    drum4.secondMix(2);
    //drum4.length(50);
    drum4.noteOn();
    trig_reverb(val_trig_reverb);
    trig_bitcrusher(val_trig_bitcrusher);
    trig_biquad(val_trig_biquad);
    //delay(200);
  }

  ///////////////////////////////////////////////////////////////////////////////////
  // Lecture analogique des potentiomètres pour déterminer volumes et panoramiques //
  ///////////////////////////////////////////////////////////////////////////////////
  
  lecture_Pot();

  ///////////////////////////////////////////////////////////////////////
  // appel aux fonctions de réglages des amplificateurs pour le volume //
  ///////////////////////////////////////////////////////////////////////

  volume_drum1(val_vol_drum1);
  volume_drum2(val_vol_Hat);
  volume_drum3(val_vol_cymbal);
  volume_drum4(val_vol_drum4);


  //////////////////////////////////////////////////////////////////////////
  //appel aux fonctions de réglage des mixers en fonction des panoramiques//
  //////////////////////////////////////////////////////////////////////////

  pano_drum(val_pano_drum1, val_pano_Hat, val_pano_cymbal, val_pano_drum4);
  vol_master(val_vol_master);
}
