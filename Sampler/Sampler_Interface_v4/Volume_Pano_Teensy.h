/**
  * @fn float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
  * @brief Permet de mapper un float d'une plage vers une autre.
  * @param x: flottant à convertir.
  * @param in_min: borne inférieure de plage d'entrée.
  * @param in_max: borne supérieure de la plage d'entrée.
  * @param out_min: borne inférieure de plage de sortie.
  * @param out_max: borne supérieure de plage de sortie.
  * @return le flottant convertit dans la nouvelle plage
  */
float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/**
  * @fn void volume_drum1(int val_vol_drum1)
  * @brief prend en paramètre la valeur du volume à partir d'un potar et règle l'ampli de la percu 1 en conséquence.
  * @param val_vol_drum1: variable lisant la valeur analogique en sortie du potar du volume de drum1.
  */
void volume_drum1 (int val_vol_drum1)
{
  float val_ampli;
  val_ampli = mapfloat(val_vol_drum1, 0 , 1023, 0, 1.0);
  REGLAGE_VOLUME_DRUM1.gain(val_ampli);
}


/**
  * @fn void volume_drum2(int val_vol_drum2)
  * @brief prend en paramètre la valeur du volume à partir d'un potar et règle l'ampli de la percu 2 en conséquence.
  * @param val_vol_drum2: variable lisant la valeur analogique en sortie du potar du volume de drum2.
  */
void volume_drum2 (int val_vol_drum2)
{
  float val_ampli;
  val_ampli = mapfloat(val_vol_drum2, 0 , 1023, 0, 1.0);
  REGLAGE_VOLUME_MUL_HAT.gain(val_ampli);
}


/**
  * @fn void volume_drum3(int val_vol_drum3)
  * @brief prend en paramètre la valeur du volume à partir d'un potar et règle l'ampli de la percu 3 en conséquence.
  * @param val_vol_drum3: variable lisant la valeur analogique en sortie du potar du volume de drum3.
  */
void volume_drum3 (int val_vol_drum3)
{
  float val_ampli;
  val_ampli = mapfloat(val_vol_drum3, 0 , 1023, 0, 1.0);
  REGLAGE_VOLUME_MUL_CYMBAL.gain(val_ampli);
}

/**
  * @fn void volume_drum4(int val_vol_drum4)
  * @brief prend en paramètre la valeur du volume à partir d'un potar et règle l'ampli de la percu 4 en conséquence.
  * @param val_vol_drum4: variable lisant la valeur analogique en sortie du potar du volume de drum4.
  */ 
void volume_drum4 (int val_vol_drum4)
{
  float val_ampli;
  val_ampli = mapfloat(val_vol_drum4, 0 , 1023, 0, 1.0);
  REGLAGE_VOLUME_DRUM4.gain(val_ampli);
}

/**
  * @fn void pano_drum(int val_pano_drum1, int val_pano_drum2, int val_pano_drum3, int val_pano_drum4)
  * @brief Permet de gérer les mixers PERCU GAUCHE et PERCU DROITE en fonction des panoramiques de chaque drum.
  * @param val_pano_drum1: variable lisant la valeur analogique en sortie du potar du pano de drum1.
  * @param val_pano_drum2: variable lisant la valeur analogique en sortie du potar du pano de drum2.
  * @param val_pano_drum3: variable lisant la valeur analogique en sortie du potar du pano de drum3.
  * @param val_pano_drum4: variable lisant la valeur analogique en sortie du potar du pano de drum4.
  */
void pano_drum(int val_pano_drum1, int val_pano_drum2, int val_pano_drum3, int val_pano_drum4)
{

  //remapper les panormariques pour la plage acceptée par les mixers
  float pano_drum1_remapped;
  pano_drum1_remapped = mapfloat(val_pano_drum1, 0 , 1023, 0, 1.0);
  float pano_drum2_remapped;
  pano_drum2_remapped = mapfloat(val_pano_drum2, 0 , 1023, 0, 1.0);
  float pano_drum3_remapped;
  pano_drum3_remapped = mapfloat(val_pano_drum3, 0 , 1023, 0, 1.0);
  float pano_drum4_remapped;
  pano_drum4_remapped = mapfloat(val_pano_drum4, 0 , 1023, 0, 1.0);

  //configuration des mixers en fonction des panoramiques
  PERCU_GAUCHE.gain(0, 1 - pano_drum1_remapped);
  PERCU_DROITE.gain(0, pano_drum1_remapped);
  PERCU_GAUCHE.gain(1, 1 - pano_drum2_remapped);
  PERCU_DROITE.gain(1, pano_drum2_remapped);
  PERCU_GAUCHE.gain(2, 1 - pano_drum3_remapped);
  PERCU_DROITE.gain(2, pano_drum3_remapped);
  PERCU_GAUCHE.gain(3, 1 - pano_drum4_remapped);
  PERCU_DROITE.gain(3, pano_drum4_remapped);
}

/**
  * @fn void vol_master(int val_vol_master)
  * @brief Permet de gérer le volume du master
  * @param val_vol_master: variable lisant la valeur analogique en sortie du potar du panoramique de master.
  */
void vol_master (int val_vol_master)
{
  float vol_master_remapped;
  vol_master_remapped = mapfloat(val_vol_master, 0 , 1023, 0, 5.0);

  MASTER_GAUCHE.gain(0, vol_master_remapped);
  MASTER_GAUCHE.gain(1, vol_master_remapped);
  MASTER_GAUCHE.gain(2, vol_master_remapped);
  
  MASTER_DROITE.gain(0, vol_master_remapped);
  MASTER_DROITE.gain(1, vol_master_remapped);
  MASTER_DROITE.gain(2, vol_master_remapped);
}
