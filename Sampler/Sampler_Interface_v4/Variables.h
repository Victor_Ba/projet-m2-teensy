//////////////// CONSTANTES ///////////////////

const int trig_drum1 = 30;
const int trig_Hat = 29;
const int trig_cymbal = 28;
const int trig_drum4 = 27;

const int trigger_reverb = 26;
const int trigger_bitcrusher = 25;
const int trigger_biquad = 24;

const int pano_drum1 = 37;
const int pano_Hat = 38;
const int pano_cymbal = 31;
const int pano_drum4 = 32;

const int volume_master = 39;
const int vol_drum1 = 36;
const int vol_Hat = 35;
const int vol_cymbal = 34;
const int vol_drum4 = 33;

// variables globales
int val_vol_drum1;
int val_pano_drum1;

int val_vol_Hat;
int val_pano_Hat;

int val_vol_cymbal;
int val_pano_cymbal;

int val_vol_drum4;
int val_pano_drum4;

int val_vol_master;

int val_trig_drum1;
int val_trig_Hat;
int val_trig_cymbal;
int val_trig_drum4;

int val_trig_reverb;
int val_trig_bitcrusher;
int val_trig_biquad;

// MIDI
MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);
byte typeMidi;
byte canalMidi;
byte noteMidi;
byte velociteMidi;

// commandes MIDI
#define MIDI_NOTE_ON 144
#define MIDI_NOTE_OFF 128

// Carte SD
#define SDCARD_CS_PIN    10
#define SDCARD_MOSI_PIN  7
#define SDCARD_SCK_PIN   14