var searchData=
[
  ['trig_5fcymbal_381',['trig_cymbal',['../_variables_8h.html#aacfe9b3e6125960d5fccb77cb2f460ad',1,'Variables.h']]],
  ['trig_5fdrum1_382',['trig_drum1',['../_variables_8h.html#aa7776b53f96b4a45452c46a140d74005',1,'Variables.h']]],
  ['trig_5fdrum4_383',['trig_drum4',['../_variables_8h.html#ab142cadd8d619e6ce92bf28185edb0d4',1,'Variables.h']]],
  ['trig_5fhat_384',['trig_Hat',['../_variables_8h.html#ae9feba85e6526f27c2e26298d78f83e0',1,'Variables.h']]],
  ['trigger_5fbiquad_385',['trigger_biquad',['../_variables_8h.html#ae17f5911d0e068e4828648bdc43171b2',1,'Variables.h']]],
  ['trigger_5fbitcrusher_386',['trigger_bitcrusher',['../_variables_8h.html#a0c75d5255f9abcb7d657616f57088528',1,'Variables.h']]],
  ['trigger_5freverb_387',['trigger_reverb',['../_variables_8h.html#a1a890b28c34ba7197652f86b797d8ac8',1,'Variables.h']]],
  ['typemidi_388',['typeMidi',['../_variables_8h.html#ab4b15504ab01b3ffd2ca0b39d9fa8cb8',1,'Variables.h']]]
];
