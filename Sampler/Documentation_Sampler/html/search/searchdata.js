var indexSectionsWithContent =
{
  0: "abcdefilmnprstuvw",
  1: "a",
  2: "frsv",
  3: "afilmnpstuvw",
  4: "abcdefimnprstvw",
  5: "e",
  6: "st",
  7: "ms"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Énumérations",
  6: "Valeurs énumérées",
  7: "Macros"
};

